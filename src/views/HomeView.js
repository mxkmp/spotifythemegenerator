import React from 'react';
import {makeStyles} from "@material-ui/styles";
import Login from "../components/Login/LoginButton";
import {initialState, PlaylistProvider, PlaylistReducer} from "../contexts/PlaylistContext";
import TrackTable from "../components/TrackTable/TrackTable";
import {useLoginValue} from "../contexts/LoginContext";
import {Grid} from '@material-ui/core';
import AllPlaylists from "../components/AllPlaylists/AllPlaylists";

const useStyles = makeStyles(theme => ({
    root: {
        // backgroundColor: 'green',
    },
}));
const HomeView = () => {
    const [login, dispatch] = useLoginValue();
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Grid container>
                <Grid item xs={12}>
                    <Login/>
                </Grid>
                <Grid item xs={12}>
                    <Grid container>
                        <PlaylistProvider initialState={initialState} reducer={PlaylistReducer}>
                            <Grid item xs={12} lg={6}>
                                <AllPlaylists/>
                            </Grid>
                            <Grid item xs={12} lg={6}>
                                <TrackTable/>
                            </Grid>
                        </PlaylistProvider>
                    </Grid>
                </Grid>
            </Grid>
        </div>)
};

export default (HomeView)