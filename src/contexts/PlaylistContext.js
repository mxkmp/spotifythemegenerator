import React, {createContext, useContext, useReducer} from "react";

export const PlaylistContext = createContext();
export const initialState = {playlist_id_set: new Set(), allTracks: [], allArtists: []}

export const SAVE_PLAYLIST_ID = 'SAVE_PLAYLIST_ID'
export const PLAYLIST_SAVE_PLAYLIST_SET = 'SAVE_PLAYLIST_SET'
export const PLAYLIST_SAVE_ALL_TRACKS = 'SAVE_ALL_TRACKS'
export const PLAYLIST_SAVE_ALL_ARTISTS = 'SAVE_ALL_TRACKS'

export const PlaylistReducer = (state, action) => {
    switch (action.type) {
        case PLAYLIST_SAVE_PLAYLIST_SET: {
            return {
                ...state,
                playlist_id_set: action.playlist_id_set
            }
        }
        case PLAYLIST_SAVE_ALL_TRACKS: {
            return {
                ...state,
                allTracks: action.allTracks
            }
        }
        case PLAYLIST_SAVE_ALL_ARTISTS: {
            return {
                ...state,
                allArtists: action.allArtists
            }
        }
        default:
            return state;
    }
};
export const PlaylistProvider = ({reducer, initialState, children}) => (
    <PlaylistContext.Provider value={useReducer(reducer, initialState)}>
        {children}
    </PlaylistContext.Provider>
);

export const usePlayListValue = () => useContext(PlaylistContext);
