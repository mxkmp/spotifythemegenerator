import React, {useContext, useReducer} from "react";
import {spotify} from "../components/utils/SpotifyApi";

const LoginContext = React.createContext({});

const state = {
    access_token: null,
    expires_in: null,
    state: null,
    token_type: null,
    login_date: null,
    user_id: null,
    display_name: null,
    email: null
};

const loginStorageKey = 'login_info'
export const initialState = JSON.parse(localStorage.getItem(loginStorageKey)) || state //TODO: für dev

if (initialState.access_token) {
    spotify.setAccessToken(initialState.access_token)
}

export const LOGIN_SAVE_ACCESS_TOKEN = 'LOGIN_SAVE_ACCESS_TOKEN'
export const LOGIN_SAVE_LOGIN_INFO = 'LOGIN_SAVE_LOGIN_INFO'
export const LOGIN_RESET_LOGIN_INFO = 'LOGIN_RESET_LOGIN_INFO'

export const LoginReducer = (state, action) => {
    switch (action.type) {
        case LOGIN_SAVE_LOGIN_INFO: {
            const obj = {
                access_token: action.access_token,
                expires_in: action.expires_in,
                state: action.state,
                token_type: action.token_type,
                login_date: action.login_date,
                user_id: action.user_id,
                display_name: action.display_name,
                email: action.email
            };
            localStorage.setItem(loginStorageKey, JSON.stringify(obj)) //TODO: verschlüsseln
            return {
                ...state,
                ...obj
            }
        }
        case LOGIN_RESET_LOGIN_INFO: {
            localStorage.removeItem(loginStorageKey)
            return {
                ...state,
                access_token: null,
                expires_in: null,
                state: null,
                token_type: null,
                login_date: null,
            }
        }
        case LOGIN_SAVE_ACCESS_TOKEN:
            return {
                ...state,
                access_token: action.access_token
            };
        default:
            return state;
    }
};

export const LoginProvider = ({reducer, initialState, children}) => (
    <LoginContext.Provider value={useReducer(reducer, initialState)}>
        {children}
    </LoginContext.Provider>
);


export const useLoginValue = () => useContext(LoginContext);

export default LoginContext;