const request = require('request-promise');
const base64 = require('base-64');

//TODO: Fehlerbehandlung
export const spotify = {
    clientId: "849d0002cfc543be9ac971a5ff247b3e",
    clientSecret: "16c0b5f8766c4bb6a1281a85244f110f",
    scope: ['playlist-read-private', 'playlist-modify-public', 'playlist-modify-private', 'playlist-read-collaborative', 'user-read-email', 'user-read-birthdate', 'user-read-private'],
    redirectUri: 'http://localhost:3000/spotify/callback',
    tokenType: 'Bearer',
    accessToken: 'BQB5vDH1n4DhfsbHOaolX5Sh4NJwQawPH2BXo2Uo9XlTTriW8ynDur2oHlORd0bFdTm1lJMDFuV-iVMQ_XHqB0QzYnobSisXZ_z7c32qziXkDRaGb5SDCMbE3sO1b6kmrNyCWrhkEd7azjx4-2TACVFUQxSyiUlrfzNS_is0jE2D7EDw8Y5cb8GvXBr9H70m8-wRpqy5WTT1mW_a2xIR6c-sRMqgAEO658vYONDd0yx6BIU',
    refreshToken: 'AQA5DCn-YX68YmcEsvtYvG7l2G_3O29cM1exebJFrz9GqYTWZT-gCaibvirYEXfxz07bDVfdK35E_hG6scbdY9SIssrEAfumU-uBEX4yyxAAQV-lDTMtei7xqMv-LfyU0TWzYA',
    userId: '1126012545'
};

const SpotifyTypes = {
    ALBUM: 'album',
    ARTIST: 'artist',
    PLAYLIST: 'currentPlaylist',
    TRACK: 'track'
};

class SpotifyHelper {

    constructor(clientId = spotify.clientId,
                clientSecret = spotify.clientSecret,
                scope = spotify.scope,
                tokenType = spotify.tokenType,
                accessToken = spotify.accessToken,
                refreshToken = spotify.refreshToken,
                userId = spotify.userId) {
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.scope = scope;
        this.tokenType = tokenType;
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
        this.userId = userId;
    }

    async handleError(err, call, ...params) {
        return new Promise((resolve, reject) => {
            if (err.statusCode === 401) { //access_token expired
                console.log(call.name, 'Refreshing access_token', call, params);
                this.refreshAccessToken().then((res) => {
                    const {access_token} = JSON.parse(res);
                    this.accessToken = access_token;
                    resolve(call.apply(this, params));
                }).catch(err => {
                    console.log(call.name, 'err', err)
                });
            } else {
                console.error('handleError', call.name, params, err.message);
                reject(err);
            }
        });
    }

    static generateLoginUrl(clientId, scope, ) {
        const url = 'https://accounts.spotify.com/authorize';

        const body = {
            client_id: clientId,
            response_type: 'code',
            state: 123,
            redirect_uri: spotify.redirectUri,
            scope: this.scope.join(' '),
            show_dialog: false
        };

        const r = request({
            method: 'GET',
            url: url,
            form: body
        });
        return url + '?' + r.body;
    }

    async requestAccessToken(code) {
        const url = 'https://accounts.spotify.com/api/token';
        const authCode = this.genAuthCodeLogin();
        return await request({
            method: 'POST',
            headers: {
                Authorization: authCode
            },
            form: {
                code: code,
                redirect_uri: spotify.redirectUri,
                grant_type: 'authorization_code'
            },
            url: url
        }).catch(err => {
            return this.handleError(err, this.requestAccessToken, Object.keys(arguments));
        });
    }

    async refreshAccessToken() {
        const url = 'https://accounts.spotify.com/api/token';
        const authCode = this.genAuthCodeLogin();

        return request({
            method: 'POST',
            headers: {
                Authorization: authCode
            },
            form: {
                grant_type: 'refresh_token',
                refresh_token: this.refreshToken
            },
            url: url
        }).catch(err => {
            console.error('refreshAccessToken', err);
        });
    }


    async createPlaylist(name, description, pub = true, collob = false) {
        const url = `https://api.spotify.com/v1/users/${this.userId}/playlists`;
        const authCode = this.genAuthCode();

        return await request({
            method: 'POST',
            headers: {
                Authorization: authCode
            },
            body: {
                name: name,
                public: pub,
                collaborative: collob,
                description: description
            },
            url: url,
            json: true
        }).catch(err => {
            return this.handleError(err, this.createPlaylist, Object.keys(arguments));
        })
    }

    async searchItem(keyword, type, limit = 50, offset = 0) {
        const url = `https://api.spotify.com/v1/search`;
        const authCode = this.genAuthCode();
        const newType = (typeof type === 'object') ? type.join(',') : type;
        const newKeyword = (typeof keyword === "object") ?
            Object.keys(keyword).map(e => {return `${e}:${keyword[e]}`}).join(" ") : keyword;

        const body = {
            q: newKeyword,
            type: newType,
            limit: limit,
            offset: offset
        };

        return request({
            method: 'GET',
            headers: {
                Authorization: authCode
            },
            qs: body,
            url: url,
        }).catch(err => {
            return this.handleError(err, this.searchItem, ...arguments);
        });
    }

    getNewestReleasesThroughSearch() {
        return this.searchItem({tag: 'new', genre:'hip hop'}, [SpotifyTypes.TRACK]);
    }

    getNextPage(url) {
        const authCode = this.genAuthCode();

        return request({
            method: 'GET',
            headers: {
                Authorization: authCode
            },
            url: url
        }).catch(err => {
            return this.handleError(err, this.getNextPage, Object.keys(arguments));
        });
    }

    getNewestReleases(uri = 'https://api.spotify.com/v1/browse/new-releases') {
        const url = uri;
        const authCode = this.genAuthCode();

        return request({
            method: 'GET',
            headers: {
                Authorization: authCode
            },
            url: url
        }).catch(err => {
            return this.handleError(err, this.getNewestReleases(), Object.keys(arguments));
        });
    }

    getAllPlaylists(limit = 20, offset = 0) {
        const url = `https://api.spotify.com/v1/users/${this.userId}/playlists`;
        const authCode = this.genAuthCode();

        return request({
            method: 'GET',
            headers: {
                Authorization: authCode
            },
            qs: {
                limit: limit,
                offset: offset
            },
            url: url
        }).catch(err => {
            return this.handleError(err, this.getAllPlaylists, Object.keys(arguments));
        });
    }

    getPlaylist(id) {
        const url = `https://api.spotify.com/v1/playlists/${id}`;
        const authCode = this.genAuthCode();

        return request({
            method: 'GET',
            headers: {
                Authorization: authCode
            },
            url: url
        }).catch(err => {
            return this.handleError(err, this.getPlaylist, Object.keys(arguments));
        });
    }

    getPlaylistTracks(id, limit = 100, offset = 0) {
        console.log('GetPlayListTracks', id, limit, offset);
        const url = `https://api.spotify.com/v1/playlists/${id}/tracks`;
        const authCode = this.genAuthCode();

        return request({
            method: 'GET',
            headers: {
                Authorization: authCode
            },
            qs: {
                limit: limit,
                offset: offset,
            },
            url: url
        }).catch(err => {
            return this.handleError(err, this.getPlaylistTracks, Object.keys(arguments));
        });
    }

    addTrackToPlaylist(playlist_id, tracks, position = 0) {
        const url = `https://api.spotify.com/v1/playlists/${playlist_id}/tracks`;
        const authCode = this.genAuthCode();

        return request({
            method: 'POST',
            headers: {
                Authorization: authCode
            },
            body: {
                uris: tracks.map( (e) => e.uri),
                position: position
            },
            url: url,
            json:true
        }).catch(err => {
            return this.handleError(err, this.addTrackToPlaylist, Object.keys(arguments));
        });
    }

    removeTracksFromPlaylist(playlist_id, tracks) {
        const url = `https://api.spotify.com/v1/playlists/${playlist_id}/tracks`;
        const authCode = this.genAuthCode();

        return request({
            method: 'DELETE',
            headers: {
                Authorization: authCode
            },
            body: {
                uris: tracks.map((e) => e.uri),
            },
            url: url,
            json: true
        }).catch(err => {
            return this.handleError(err, this.removeTracksFromPlaylist, Object.keys(arguments));
        });
    }

    changePlaylistDetail(id, name, description,  pub = true, collob = false) {
        const url = `https://api.spotify.com/v1/playlists/${id}`;
        const authCode = this.genAuthCode();

        const body = {
            name: name,
            public: pub,
            collaborative: collob,
            description: description
        };

        return request({
            method: 'PUT',
            headers: {
                Authorization: authCode
            },
            body: body,
            json: true,
            url: url
        }).catch(err => {
            return this.handleError(err, this.changePlaylistDetail, Object.keys(arguments));
        });
    }

    removeTracksFromPlaylist(id, tracks) {
        const url = `https://api.spotify.com/v1/playlists/${id}/tracks`;
        const authCode = this.genAuthCode();
        const t = tracks.map(e => {
            return {uri: e.uri}
        });

        return request({
            method: 'DELETE',
            headers: {
                Authorization: authCode
            },
            url: url,
            body: {
                tracks: t,
            },
            json: true
        }).catch(err => {
            return this.handleError(err, this.removeTracksFromPlaylist, Object.keys(arguments));
        })
    }

    getTracksFromPlaylist(id) {
        const url =`https://api.spotify.com/v1/playlists/${id}/tracks`;
        const authCode = this.genAuthCode();

        return request({
            method: 'GET',
            headers: {
                Authorization: authCode
            },
            url: url
        }).catch(err => {
            return this.handleError(err, this.getTracksFromPlaylist, Object.keys(arguments));
        });
    }

    getAlbumMetadata(id) {
        const url = `https://api.spotify.com/v1/albums/${id}`
        const authCode = this.genAuthCode();


        return request({
            method: 'GET',
            headers: {
                Authorization: authCode
            },
            url: url
        }).catch(err => {
            return this.handleError(err, this.getAlbumMetadata, Object.keys(arguments));
        });
    }

    getArtistMetadata(id) {
        const url = `https://api.spotify.com/v1/artists/${id}`;
        const authCode = this.genAuthCode();

        return request({
            method: 'GET',
            headers: {
                Authorization: authCode
            },
            url: url
        }).catch(err => {
            return this.handleError(err, this.getArtistMetadata, Object.keys(arguments));
        });
    }

    getSeveralArtistsMetadata(ids) {
        const url = `https://api.spotify.com/v1/artists`;
        const authCode = this.genAuthCode();

        return request({
            method: 'GET',
            headers: {
                Authorization: authCode
            },
            qs: {
                ids: ids.join(',')
            },
            url: url
        }).catch(err => {
            return this.handleError(err, this.getSeveralArtistsMetadata, Object.keys(arguments));
        });
    }

    getRelatedArtists(artist_id) {
        const url = `https://api.spotify.com/v1/artists/${artist_id}/related-artists`;
        const authCode = this.genAuthCode();

        return request({
            method: 'GET',
            headers: {
                Authorization: authCode
            },
            url: url
        }).catch(err => {
            return this.handleError(err, this.getRelatedArtists, Object.keys(arguments));
        });
    }

    getTracksFromAlbum(id) {
        const url = `https://api.spotify.com/v1/albums/${id}/tracks`;
        const authCode = this.genAuthCode();

        return request({
            method: 'GET',
            headers: {
                Authorization: authCode
            },
            url: url
        }).catch(err => {
            return this.handleError(err, this.getTracksFromAlbum, Object.keys(arguments));
        });
    }

    getMetadataOfSeveralTracks(ids) {
        const url = `https://api.spotify.com/v1/tracks`;
        const authCode = this.genAuthCode();

        return request({
            method: 'GET',
            headers: {
                Authorization: authCode
            },
            qs: {
                ids: ids.join(',')
            },
            url: url
        }).catch(err => {
            return this.handleError(err, this.getMetadataOfSeveralTracks, Object.keys(arguments));
        });
    }

    requestUserinformation() {
        const url = 'https://api.spotify.com/v1/me';
        const authCode = this.genAuthCode();

        return request({
            method: 'GET',
            url: url,
            headers: {
                Authorization: authCode
            },
        }).catch(err => {
            return this.handleError(err, this.requestUserinformation, Object.keys(arguments));
        })
    }

    genAuthCodeLogin() {
        return `Basic ${base64.encode(this.clientId + ":" + this.clientSecret)}`;
    }

    genAuthCode() {
        return `${this.tokenType} ${this.accessToken}`;
    }

    /**
     * fetches multiple data depending on offsets and limit
     * @param func function to execute
     * @param params parameters of the function
     * @returns {Promise<Array|T[]>}
     */
    async fetchData(callback) {
        let allItems = [];
        let size = 0;
        let pLimit = 50;
        let offset = 0;
        let totalSize = 9999999;
        console.log('fetchData', callback.name, pLimit, offset);
        do {
            //TODO: anpassen bei anderen funktionen
            const res = await callback(pLimit, offset);
            const {items, limit, total} = res;

            totalSize = total;

            if ((offset + limit) > total) {
                offset = totalSize - limit;
            } else {
                offset += limit;
            }

            size += offset;
            allItems = allItems.concat(items);
            console.warn('fetched', `${allItems.length}/${total}`);
        } while (allItems.length < totalSize);

        return allItems;
    }

    /**
     * fetches data for multipe ids that are limited by a maximum.
     *
     * first argument of the function has to be the array of the ids to fetch
     *
     * @param arr  data to fetch
     * @param chunkSize
     * @param callback (ids) as param has to be return a object format
     * @returns array[]
     */
    async fetchArrayOfInformations(arr, chunkSize = 50, callback) {
        const results = [];
        for (let i = 0; i < arr.length; i += chunkSize) {
            const chunk = arr.slice(i, i + chunkSize);
            const res = await callback(chunk);
            results.push(res);
        }

        return results;
    }


    /**
     *
     * @param data
     * @param callback
     * @returns {Promise<Array>}
     */
    async iterateChunks(data, callback) {
        const res = [];
        if (data.length <= 100) {
            for (let i = 0; i < data.length; i += 100) {
                const chunk = data.slice(i, i + 99)
                res.push(JSON.stringify(await callback(chunk)));
            }
        } else {
            res.push(callback(data));
        }
        return res;
    }
}

module.exports = {
    SpotifyHelper: SpotifyHelper,
    SpotifyTypes: SpotifyTypes
};