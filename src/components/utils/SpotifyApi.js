import SpotifyWebApi from 'spotify-web-api-js'


export const fetchSpotifyData = async (func, params, accessor) => {
    return new Promise(async (resolve, reject) => {
        function getAccessor(data) {
            return (accessor) ? data[accessor].items : data.items;
        }

        let allItems = [];
        const res = await func.apply(this, params)
        if (!res) {
            reject(`data of ${func.name} ({${params}) is null`)
        }
        let {next, limit, total, offset} = res;
        const data = getAccessor(res)
        allItems = allItems.concat(data);
        offset += limit;
        for (let i = offset; i < total; i += limit) {
            const res = await spotify.getGeneric(next);
            next = res.next;
            allItems = allItems.concat(getAccessor(res))
        }
        resolve(allItems);
    });

}

SpotifyWebApi.prototype.fetchData = fetchSpotifyData
export const spotify = new SpotifyWebApi();

