import React, {useEffect, useState} from 'react';
import {makeStyles} from "@material-ui/styles";
import {PLAYLIST_SAVE_ALL_TRACKS, usePlayListValue} from "../../contexts/PlaylistContext";
import {Table, TableBody, TableCell, TableHeader, TableRow} from "semantic-ui-react";
import {useLoginValue} from "../../contexts/LoginContext";
import {spotify} from "../utils/SpotifyApi";
import {ownTheme} from "../../theme";

//TODO: cashing
const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        maxHeight: ownTheme.root.viewHeight + 'vh',
        overflowY: 'scroll',
        overflowX: 'auto'
    },
}));
const HEADER = ['Name', 'Artist', "Album", "Genre"];

const trackMap = new Map()


const TrackTable = () => {
    const [playlist, dispatch] = usePlayListValue()
    const [login] = useLoginValue()

    const classes = useStyles();
    const [dataSet, setDataSet] = useState(playlist.playlist_id_set)
    const [allTracks, setAllTracks] = useState()
    const [data, setData] = useState(null);

    useEffect(() => {
        if (!playlist.playlist_id_set) return

        setDataSet(playlist.playlist_id_set)
    }, [playlist.playlist_id_set.size])

    useEffect(() => {
        /**
         * checks all playlists if they are already in the track map
         * @param set
         * @returns {Promise<any[]>} returns list of ids that are not longer valid
         */
        async function checkSet(set) { //TODO: alles rauslösen Table soll nur auf Daten hören. Verarbeitung muss woanders stattfinden, da die Tabelle nicht angezeigt wird.
            const allIds = Array.from(trackMap.keys())

            for (const value of set) {
                if (trackMap.has(value)) {
                    allIds.splice(allIds.indexOf(value), 1)
                    continue
                }

                const res = await spotify.fetchData(spotify.getPlaylist, [value, {limit: 50}], "tracks")
                    .catch(err => {
                        console.error('Fetch playlists', err)
                    })
                trackMap.set(value, res)
            }
            return allIds
        }

        if (!dataSet) return

        checkSet(dataSet).then((res) => {
            //cleanup
            for (const r of res) {
                trackMap.delete(r)
            }
            let allTracks = []
            for (const tracks of trackMap.values()) {
                allTracks.push(...tracks)
            }
            dispatch({type: PLAYLIST_SAVE_ALL_TRACKS, allTracks: allTracks})
            setData(formatData(allTracks))
        })
    }, [dataSet.size])

    const onClickHandler = (event, e) => {

    }
    const formatData = (datas) => {
        const formatToHtml = (data) => {
            const {name, album, artists} = data.track;
            const elems = [name, artists.map(e => e.name).join(","), album.name, []];

            return elems.map((e, i) => {
                return <TableCell key={e + i}>{e}</TableCell>
            });
        };
        const result = [];
        datas.forEach((e, i) => {

            const e1 = formatToHtml(e)
            const row = (<TableRow onClick={(event) => onClickHandler(event, e)} key={e + i}>{e1}</TableRow>)
            result.push(row);
        });

        return result;
    };

    return (
        <div className={classes.root}>
            <Table celled selectable>
                <TableHeader>
                    <TableRow>
                        {HEADER.map((e, i) => <Table.HeaderCell key={e + i}>{e}</Table.HeaderCell>)}
                    </TableRow>
                </TableHeader>
                <TableBody>
                    {data}
                </TableBody>
            </Table>
        </div>

    )
};

export default TrackTable;
