import React, {useEffect} from 'react';
import {Card, CardContent, CardHeader, CardMeta} from "semantic-ui-react";

const Track = ({album, artists, name, id} = this.props) => {

    useEffect(() => {
        console.log('props', album, artists, name, id);
    }, [])

    //{title} {artists.map( (e) => e.name)} {album.name}

    return (
        <Card>
            <CardContent>
                <CardHeader>
                    {name}
                </CardHeader>
                <CardMeta>
                    {artists.map((e) => e.name).join(",")}
                </CardMeta>
            </CardContent>
        </Card>

    );
};

export default Track;
