import React from 'react';
import {Redirect, Route, Switch, withRouter} from "react-router";
import {BrowserRouter} from "react-router-dom";
import LoginView from "../../views/LoginView";

const Router = (props) => {
    return (
        <BrowserRouter>
            <Switch>
                <Route exact path={"/login"} component={LoginView}/>
                <Redirect to={"/login"}/>
            </Switch>
        </BrowserRouter>
    );
};

export default withRouter(Router);
