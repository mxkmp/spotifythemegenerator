import React from 'react';
import {Redirect, Route} from "react-router";
import {LOGIN_RESET_LOGIN_INFO, useLoginValue} from "../../contexts/LoginContext";

export const AuthHandler = {
    authenticate(login, dispatch) {
        //TODO:
    },
    isAuthenticated(login) {
        return (login.access_token)
    },
    signOut(dispatch) {
        dispatch({type: LOGIN_RESET_LOGIN_INFO})
    }
};

const PrivateRoute = ({component: Component, ...rest}) => {
    const [login] = useLoginValue()
    return (
        <Route {...rest} render={(props) => {
            return AuthHandler.isAuthenticated(login) ? <Component {...props}/> : <Redirect to={'/login'}/>
        }}/>
    )
};

export default PrivateRoute;
