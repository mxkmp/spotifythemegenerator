import React, {useEffect, useState} from 'react';
import List from "@material-ui/core/List";
import PlaylistItem from "./PlaylistItem";
import {makeStyles} from "@material-ui/styles";
import Box from "@material-ui/core/Box";
import {spotify} from "../utils/SpotifyApi";
import {useLoginValue} from "../../contexts/LoginContext";
import {PLAYLIST_SAVE_PLAYLIST_SET, usePlayListValue} from "../../contexts/PlaylistContext";
import {ownTheme} from "../../theme";


const useStyles = makeStyles(theme => ({
    root: {
        maxHeight: ownTheme.root.viewHeight + 'vh',
        overflowY: 'scroll',
        overflowX: 'auto'
    },
}));

const AllPlaylists = (props) => {

    const [playlist, dispatch] = usePlayListValue();
    const [login] = useLoginValue()
    const [list, setList] = useState([])
    const idSet = new Set();

    const classes = useStyles()

    useEffect(() => {
        //setList(formatData(Playlists, clickHandler))
    }, [])

    useEffect(() => {
        if (login.access_token && login.user_id) {
            spotify.fetchData(spotify.getUserPlaylists, [login.user_id, {limit: 50}]).then((tracks) => {
                tracks.sort((a, b) => {
                    const aName = a.name.toLowerCase()
                    const bName = b.name.toLowerCase()
                    if (aName > bName) return 1
                    else if (aName < bName) return -1
                    else return 0
                })
                setList(formatData(tracks, clickHandler))
            }).catch((err) => {
                console.error('loginhandler', err)
            })
        }
    }, [login.access_token, login.user_id]);

    const clickHandler = (value) => {
        if (value.checked) {
            idSet.add(value.id)
        } else {
            idSet.delete(value.id)
        }
        dispatch({type: PLAYLIST_SAVE_PLAYLIST_SET, playlist_id_set: idSet})
    }

    const formatData = (array, clickHandler) => {
        const result = [];
        return array.map((e, i) => {
            return <PlaylistItem {...e} onClick={clickHandler} key={e.name + i}/>
        })
    };

    return (
        <Box height="100%">
            <List dense className={classes.root}>
                {list}
            </List>
        </Box>

    );
};


export default AllPlaylists;
