import React, {useEffect, useState} from 'react';
import {Checkbox, ListItem, ListItemAvatar, ListItemText, Typography} from "@material-ui/core";
import Avatar from "@material-ui/core/Avatar";
import {makeStyles} from "@material-ui/styles";


const useStyles = makeStyles(theme => ({
    root: {},
}));

const PlaylistItem = ({id, name, owner, tracks, images, external_urls, type, onClick} = this.props) => {

    const [image, setImage] = useState({url: null})
    const [checked, setChecked] = useState(false)
    const classes = useStyles()

    useEffect(() => {
        //get smallest picture
        if (images.length > 1) {
            const heights = images.map(e => e.height)
            const minIdx = heights.indexOf(Math.min.apply(Math, heights));
            setImage(images[minIdx])
        } else if (images.length === 1) {
            setImage(images[0])
        }
    }, [])

    useEffect(() => {
        onClick({checked, id})
    }, [checked])

    const onChangeSelect = (event) => {
        if (event.target === null || event.target.checked === null) return
        setChecked(event.target.checked)
    }

    const onClickItem = (event) => {
        setChecked(!checked)
    }

    return (
        <div>
            <ListItem alignItems="flex-start" divider onClick={onClickItem}>
                <ListItemAvatar>
                    <Avatar src={image.url}/>
                </ListItemAvatar>
                <ListItemText primary={name} secondary={
                    <React.Fragment>
                        <Typography component={"span"} variant={"body2"}>
                            from {owner.display_name}, Tracks: {tracks.total}
                        </Typography>
                    </React.Fragment>
                }/>
                <Checkbox edge={"start"} checked={checked} onChange={onChangeSelect}>
                </Checkbox>
            </ListItem>
        </div>

    );
};

PlaylistItem.propTypes = {};

export default PlaylistItem;
