export const Playlists =
  [
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/5Xr6QWBlAXHcU4uWyy5ICh"
            },
            "href": "https://api.spotify.com/v1/playlists/5Xr6QWBlAXHcU4uWyy5ICh",
            "id": "5Xr6QWBlAXHcU4uWyy5ICh",
            "images": [
                {
                    "height": 640,
                    "url": "https://mosaic.scdn.co/640/338b7ff72058e1a2907e16abe708fb58fed100d4406e6359a96397f865fe4fb2000ce1ba67238c2e92a8060a905faf5eea7d18660370ae00b1d0d3b1be1b20d4292ddca6348017920808f5bd04439587",
                    "width": 640
                },
                {
                    "height": 300,
                    "url": "https://mosaic.scdn.co/300/338b7ff72058e1a2907e16abe708fb58fed100d4406e6359a96397f865fe4fb2000ce1ba67238c2e92a8060a905faf5eea7d18660370ae00b1d0d3b1be1b20d4292ddca6348017920808f5bd04439587",
                    "width": 300
                },
                {
                    "height": 60,
                    "url": "https://mosaic.scdn.co/60/338b7ff72058e1a2907e16abe708fb58fed100d4406e6359a96397f865fe4fb2000ce1ba67238c2e92a8060a905faf5eea7d18660370ae00b1d0d3b1be1b20d4292ddca6348017920808f5bd04439587",
                    "width": 60
                }
            ],
            "name": "Undefined 2019",
            "owner": {
                "display_name": "Max Kamp",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/1126012545"
                },
                "href": "https://api.spotify.com/v1/users/1126012545",
                "id": "1126012545",
                "type": "user",
                "uri": "spotify:user:1126012545"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "MzIxLGJkMzA5OTk5ZGIyN2ZkZjEwOWQxZTZhNDQxYmZiOTVjZTUyMzA0YWM=",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/5Xr6QWBlAXHcU4uWyy5ICh/tracks",
                "total": 10
            },
            "type": "playlist",
            "uri": "spotify:playlist:5Xr6QWBlAXHcU4uWyy5ICh"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/2QDJENftGnnYrjDzMLJKV7"
            },
            "href": "https://api.spotify.com/v1/playlists/2QDJENftGnnYrjDzMLJKV7",
            "id": "2QDJENftGnnYrjDzMLJKV7",
            "images": [
                {
                    "height": 640,
                    "url": "https://mosaic.scdn.co/640/210fa0b8a234b12f66c05c2426dd5a8a23d62dfb252556031aa69d3a5bbeab84fbb2b5ea0429f8b95f685ee4955bd02928e49455d5a23e1613c6ef406f502397813f0ced44e9f63b7b20087c543af092",
                    "width": 640
                },
                {
                    "height": 300,
                    "url": "https://mosaic.scdn.co/300/210fa0b8a234b12f66c05c2426dd5a8a23d62dfb252556031aa69d3a5bbeab84fbb2b5ea0429f8b95f685ee4955bd02928e49455d5a23e1613c6ef406f502397813f0ced44e9f63b7b20087c543af092",
                    "width": 300
                },
                {
                    "height": 60,
                    "url": "https://mosaic.scdn.co/60/210fa0b8a234b12f66c05c2426dd5a8a23d62dfb252556031aa69d3a5bbeab84fbb2b5ea0429f8b95f685ee4955bd02928e49455d5a23e1613c6ef406f502397813f0ced44e9f63b7b20087c543af092",
                    "width": 60
                }
            ],
            "name": "Trap 2019",
            "owner": {
                "display_name": "Max Kamp",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/1126012545"
                },
                "href": "https://api.spotify.com/v1/users/1126012545",
                "id": "1126012545",
                "type": "user",
                "uri": "spotify:user:1126012545"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "NSxhMzA5YjIxNDcyMzg4ODAzNjc4MTAwOWViNDk0M2IyZmVmNTJjZTYx",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/2QDJENftGnnYrjDzMLJKV7/tracks",
                "total": 152
            },
            "type": "playlist",
            "uri": "spotify:playlist:2QDJENftGnnYrjDzMLJKV7"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/1mNbMYJMeDOlp4mLkSl3Wk"
            },
            "href": "https://api.spotify.com/v1/playlists/1mNbMYJMeDOlp4mLkSl3Wk",
            "id": "1mNbMYJMeDOlp4mLkSl3Wk",
            "images": [
                {
                    "height": 640,
                    "url": "https://mosaic.scdn.co/640/0b1e83383de772048649b850b05529f7d0b237795166bc7d61f3177d87f60e3c6aae4d62aa8a84a86997e9c594d9b1347f85241a2dded8ffb5752c63a0b8ff14e272c79221ff1924f109eee9e01432a4",
                    "width": 640
                },
                {
                    "height": 300,
                    "url": "https://mosaic.scdn.co/300/0b1e83383de772048649b850b05529f7d0b237795166bc7d61f3177d87f60e3c6aae4d62aa8a84a86997e9c594d9b1347f85241a2dded8ffb5752c63a0b8ff14e272c79221ff1924f109eee9e01432a4",
                    "width": 300
                },
                {
                    "height": 60,
                    "url": "https://mosaic.scdn.co/60/0b1e83383de772048649b850b05529f7d0b237795166bc7d61f3177d87f60e3c6aae4d62aa8a84a86997e9c594d9b1347f85241a2dded8ffb5752c63a0b8ff14e272c79221ff1924f109eee9e01432a4",
                    "width": 60
                }
            ],
            "name": "Electro 2019",
            "owner": {
                "display_name": "Max Kamp",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/1126012545"
                },
                "href": "https://api.spotify.com/v1/users/1126012545",
                "id": "1126012545",
                "type": "user",
                "uri": "spotify:user:1126012545"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "NCw0YzU5OThhMjkyN2FhMmRhOTkwMjI1M2ZjNDdlNzI1ZjkwYThlZmU2",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/1mNbMYJMeDOlp4mLkSl3Wk/tracks",
                "total": 48
            },
            "type": "playlist",
            "uri": "spotify:playlist:1mNbMYJMeDOlp4mLkSl3Wk"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/4SBiglGcH9w8ZhRZeJls98"
            },
            "href": "https://api.spotify.com/v1/playlists/4SBiglGcH9w8ZhRZeJls98",
            "id": "4SBiglGcH9w8ZhRZeJls98",
            "images": [
                {
                    "height": 640,
                    "url": "https://mosaic.scdn.co/640/307d7a28eac823562b1b65be4f08f3a949ef54e7ab67616d0000b27370ee98adddb9d63fe2573161d5a7898f85a2d20432176ad49cd8aed8b4406021e92b5ac19289be2da377082a0df648c7eb9e2dc2",
                    "width": 640
                },
                {
                    "height": 300,
                    "url": "https://mosaic.scdn.co/300/307d7a28eac823562b1b65be4f08f3a949ef54e7ab67616d0000b27370ee98adddb9d63fe2573161d5a7898f85a2d20432176ad49cd8aed8b4406021e92b5ac19289be2da377082a0df648c7eb9e2dc2",
                    "width": 300
                },
                {
                    "height": 60,
                    "url": "https://mosaic.scdn.co/60/307d7a28eac823562b1b65be4f08f3a949ef54e7ab67616d0000b27370ee98adddb9d63fe2573161d5a7898f85a2d20432176ad49cd8aed8b4406021e92b5ac19289be2da377082a0df648c7eb9e2dc2",
                    "width": 60
                }
            ],
            "name": "Hip Hop 2019",
            "owner": {
                "display_name": "Max Kamp",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/1126012545"
                },
                "href": "https://api.spotify.com/v1/users/1126012545",
                "id": "1126012545",
                "type": "user",
                "uri": "spotify:user:1126012545"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "NywxOTU1MjA0ZmFmYjgzMDhjYjU2ZWFlNTY2NGY4YWYzODJkNDJlNmYz",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/4SBiglGcH9w8ZhRZeJls98/tracks",
                "total": 306
            },
            "type": "playlist",
            "uri": "spotify:playlist:4SBiglGcH9w8ZhRZeJls98"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/3f7xR1WDKQIkIasmNfI6QO"
            },
            "href": "https://api.spotify.com/v1/playlists/3f7xR1WDKQIkIasmNfI6QO",
            "id": "3f7xR1WDKQIkIasmNfI6QO",
            "images": [
                {
                    "height": 640,
                    "url": "https://mosaic.scdn.co/640/0b1e83383de772048649b850b05529f7d0b23779779005b016afb5d226f0ecd27704b434abffcfa877eb7c17cafe5503e1c988f3eed6b57ca1e8cd88c1c1f28372c06917b8df5f1b6d9c23ea963cab4a",
                    "width": 640
                },
                {
                    "height": 300,
                    "url": "https://mosaic.scdn.co/300/0b1e83383de772048649b850b05529f7d0b23779779005b016afb5d226f0ecd27704b434abffcfa877eb7c17cafe5503e1c988f3eed6b57ca1e8cd88c1c1f28372c06917b8df5f1b6d9c23ea963cab4a",
                    "width": 300
                },
                {
                    "height": 60,
                    "url": "https://mosaic.scdn.co/60/0b1e83383de772048649b850b05529f7d0b23779779005b016afb5d226f0ecd27704b434abffcfa877eb7c17cafe5503e1c988f3eed6b57ca1e8cd88c1c1f28372c06917b8df5f1b6d9c23ea963cab4a",
                    "width": 60
                }
            ],
            "name": "Techno 2019",
            "owner": {
                "display_name": "Max Kamp",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/1126012545"
                },
                "href": "https://api.spotify.com/v1/users/1126012545",
                "id": "1126012545",
                "type": "user",
                "uri": "spotify:user:1126012545"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "MywxOWZiNWQ3ZTVhZWUyZTkwM2RmOGMwMzAxNGM2OTY5NDk1MDc3NDFh",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/3f7xR1WDKQIkIasmNfI6QO/tracks",
                "total": 28
            },
            "type": "playlist",
            "uri": "spotify:playlist:3f7xR1WDKQIkIasmNfI6QO"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/5hrrF9mKrgy8fOupd9GHh1"
            },
            "href": "https://api.spotify.com/v1/playlists/5hrrF9mKrgy8fOupd9GHh1",
            "id": "5hrrF9mKrgy8fOupd9GHh1",
            "images": [
                {
                    "height": 640,
                    "url": "https://mosaic.scdn.co/640/4104d2b93bfaafbe601181ace42694c384a48b015b1bcc9684fd5e407a0c07fdd8f5e15227dc7bb762c618518e26fd08b991d44c0c2c450ee712fa0ff8ca5bb0d3c4d8a06bb24856711baa69c6ebe748",
                    "width": 640
                },
                {
                    "height": 300,
                    "url": "https://mosaic.scdn.co/300/4104d2b93bfaafbe601181ace42694c384a48b015b1bcc9684fd5e407a0c07fdd8f5e15227dc7bb762c618518e26fd08b991d44c0c2c450ee712fa0ff8ca5bb0d3c4d8a06bb24856711baa69c6ebe748",
                    "width": 300
                },
                {
                    "height": 60,
                    "url": "https://mosaic.scdn.co/60/4104d2b93bfaafbe601181ace42694c384a48b015b1bcc9684fd5e407a0c07fdd8f5e15227dc7bb762c618518e26fd08b991d44c0c2c450ee712fa0ff8ca5bb0d3c4d8a06bb24856711baa69c6ebe748",
                    "width": 60
                }
            ],
            "name": "Deutschrap 2019",
            "owner": {
                "display_name": "Max Kamp",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/1126012545"
                },
                "href": "https://api.spotify.com/v1/users/1126012545",
                "id": "1126012545",
                "type": "user",
                "uri": "spotify:user:1126012545"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "NixhZmZjZTUxYmQxNGI2NTc4NTg5MWRlOTU3MWU0NjZlOWQwNzU4YjZi",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/5hrrF9mKrgy8fOupd9GHh1/tracks",
                "total": 258
            },
            "type": "playlist",
            "uri": "spotify:playlist:5hrrF9mKrgy8fOupd9GHh1"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/0IGP750arYJwZegRFtETBE"
            },
            "href": "https://api.spotify.com/v1/playlists/0IGP750arYJwZegRFtETBE",
            "id": "0IGP750arYJwZegRFtETBE",
            "images": [
                {
                    "height": 640,
                    "url": "https://mosaic.scdn.co/640/5ec198d5d1f84077566651a5a6ece6ba6805c325797e44f82624382082a6cb970e3dea75657872d8920fb78c159b3e0f994b6c144340be9d4a7ba6fea6b9e2a9399d45b43efd07739b3a2c6a4306076d",
                    "width": 640
                },
                {
                    "height": 300,
                    "url": "https://mosaic.scdn.co/300/5ec198d5d1f84077566651a5a6ece6ba6805c325797e44f82624382082a6cb970e3dea75657872d8920fb78c159b3e0f994b6c144340be9d4a7ba6fea6b9e2a9399d45b43efd07739b3a2c6a4306076d",
                    "width": 300
                },
                {
                    "height": 60,
                    "url": "https://mosaic.scdn.co/60/5ec198d5d1f84077566651a5a6ece6ba6805c325797e44f82624382082a6cb970e3dea75657872d8920fb78c159b3e0f994b6c144340be9d4a7ba6fea6b9e2a9399d45b43efd07739b3a2c6a4306076d",
                    "width": 60
                }
            ],
            "name": "Archiv 2019",
            "owner": {
                "display_name": "Max Kamp",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/1126012545"
                },
                "href": "https://api.spotify.com/v1/users/1126012545",
                "id": "1126012545",
                "type": "user",
                "uri": "spotify:user:1126012545"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "MTAsNDJhYmU4YTM1ZjVkYTM2NjU0MWU0YTUxZTkwY2FiNDFiZDQ2NzJmNQ==",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/0IGP750arYJwZegRFtETBE/tracks",
                "total": 629
            },
            "type": "playlist",
            "uri": "spotify:playlist:0IGP750arYJwZegRFtETBE"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/5AnHMBxH9yLfhnLcOsTAsN"
            },
            "href": "https://api.spotify.com/v1/playlists/5AnHMBxH9yLfhnLcOsTAsN",
            "id": "5AnHMBxH9yLfhnLcOsTAsN",
            "images": [
                {
                    "height": 640,
                    "url": "https://mosaic.scdn.co/640/2b907d2b04dc53602e09d11b555af95f05185d006cfe830816010005c1ac329af43fb0d4a19765a298a3e022f696d9cd49596e6569390cecb5006c5df4a026687e1b1e038db7e7716fe18ad3cddf145f",
                    "width": 640
                },
                {
                    "height": 300,
                    "url": "https://mosaic.scdn.co/300/2b907d2b04dc53602e09d11b555af95f05185d006cfe830816010005c1ac329af43fb0d4a19765a298a3e022f696d9cd49596e6569390cecb5006c5df4a026687e1b1e038db7e7716fe18ad3cddf145f",
                    "width": 300
                },
                {
                    "height": 60,
                    "url": "https://mosaic.scdn.co/60/2b907d2b04dc53602e09d11b555af95f05185d006cfe830816010005c1ac329af43fb0d4a19765a298a3e022f696d9cd49596e6569390cecb5006c5df4a026687e1b1e038db7e7716fe18ad3cddf145f",
                    "width": 60
                }
            ],
            "name": "Dubolt Mix: LX / Gzuz / Luciano / Samra",
            "owner": {
                "display_name": "Max Kamp",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/1126012545"
                },
                "href": "https://api.spotify.com/v1/users/1126012545",
                "id": "1126012545",
                "type": "user",
                "uri": "spotify:user:1126012545"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "MiwwZjM1NmI2MWVlNTBhN2FkOWMwZDNhZjdiYmNkNTc1OTU1NzRiNDlm",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/5AnHMBxH9yLfhnLcOsTAsN/tracks",
                "total": 100
            },
            "type": "playlist",
            "uri": "spotify:playlist:5AnHMBxH9yLfhnLcOsTAsN"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/3C6FEEviwMQch0W3yfVvB8"
            },
            "href": "https://api.spotify.com/v1/playlists/3C6FEEviwMQch0W3yfVvB8",
            "id": "3C6FEEviwMQch0W3yfVvB8",
            "images": [
                {
                    "height": 640,
                    "url": "https://mosaic.scdn.co/640/560c1b0a567764b2130630eb82bd4db9b5069f2f84ab060384d5ff1f95dc12937ccc799e7941cac1ab67616d0000b27393ba2fddfc734b275c06a662fb18c47e15499c1194ecfbb719ac4a07a715a2e4",
                    "width": 640
                },
                {
                    "height": 300,
                    "url": "https://mosaic.scdn.co/300/560c1b0a567764b2130630eb82bd4db9b5069f2f84ab060384d5ff1f95dc12937ccc799e7941cac1ab67616d0000b27393ba2fddfc734b275c06a662fb18c47e15499c1194ecfbb719ac4a07a715a2e4",
                    "width": 300
                },
                {
                    "height": 60,
                    "url": "https://mosaic.scdn.co/60/560c1b0a567764b2130630eb82bd4db9b5069f2f84ab060384d5ff1f95dc12937ccc799e7941cac1ab67616d0000b27393ba2fddfc734b275c06a662fb18c47e15499c1194ecfbb719ac4a07a715a2e4",
                    "width": 60
                }
            ],
            "name": "Pumpen",
            "owner": {
                "display_name": "Max Kamp",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/1126012545"
                },
                "href": "https://api.spotify.com/v1/users/1126012545",
                "id": "1126012545",
                "type": "user",
                "uri": "spotify:user:1126012545"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "NyxiM2E4YjIwYjI3NTM5MjMwMmQzZDQ4Mjk2N2I0ODQ3ODFiZWQ5ZDQx",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/3C6FEEviwMQch0W3yfVvB8/tracks",
                "total": 6
            },
            "type": "playlist",
            "uri": "spotify:playlist:3C6FEEviwMQch0W3yfVvB8"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/3BSYPuw2TKTYZBmO6eP17A"
            },
            "href": "https://api.spotify.com/v1/playlists/3BSYPuw2TKTYZBmO6eP17A",
            "id": "3BSYPuw2TKTYZBmO6eP17A",
            "images": [],
            "name": "YouTube Likes",
            "owner": {
                "display_name": "Max Kamp",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/1126012545"
                },
                "href": "https://api.spotify.com/v1/users/1126012545",
                "id": "1126012545",
                "type": "user",
                "uri": "spotify:user:1126012545"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "MSw1ZDNkYTEzMWQxNzAwZmIxYmZkNDU2MTA1NzFiMWQzZjllMTFhMjdh",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/3BSYPuw2TKTYZBmO6eP17A/tracks",
                "total": 0
            },
            "type": "playlist",
            "uri": "spotify:playlist:3BSYPuw2TKTYZBmO6eP17A"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/09bYERTQzlMY40q12Klgec"
            },
            "href": "https://api.spotify.com/v1/playlists/09bYERTQzlMY40q12Klgec",
            "id": "09bYERTQzlMY40q12Klgec",
            "images": [],
            "name": "SoundCloud Likes",
            "owner": {
                "display_name": "Max Kamp",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/1126012545"
                },
                "href": "https://api.spotify.com/v1/users/1126012545",
                "id": "1126012545",
                "type": "user",
                "uri": "spotify:user:1126012545"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "MSw0MjJlN2EwMzVjY2M5YWFiNmUyNWI1ZDYxZGE1ODMxZDk4MTUzNzYx",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/09bYERTQzlMY40q12Klgec/tracks",
                "total": 0
            },
            "type": "playlist",
            "uri": "spotify:playlist:09bYERTQzlMY40q12Klgec"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/37i9dQZEVXcLW0mrxQda0o"
            },
            "href": "https://api.spotify.com/v1/playlists/37i9dQZEVXcLW0mrxQda0o",
            "id": "37i9dQZEVXcLW0mrxQda0o",
            "images": [
                {
                    "height": null,
                    "url": "https://pl.scdn.co/images/pl/default/e321e29ec6c85bb17b84a87ffec6db420573d094",
                    "width": null
                }
            ],
            "name": "Discover Weekly",
            "owner": {
                "display_name": "Spotify",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/spotify"
                },
                "href": "https://api.spotify.com/v1/users/spotify",
                "id": "spotify",
                "type": "user",
                "uri": "spotify:user:spotify"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "MjYxNTMyODAsMDAwMDAwMDAzMmZlNWMzOTU1ZTE2MzA3YTE0NTY4ZWJiZjk0YWY4Yw==",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/37i9dQZEVXcLW0mrxQda0o/tracks",
                "total": 30
            },
            "type": "playlist",
            "uri": "spotify:playlist:37i9dQZEVXcLW0mrxQda0o"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/37i9dQZEVXblBkdFy6OJz8"
            },
            "href": "https://api.spotify.com/v1/playlists/37i9dQZEVXblBkdFy6OJz8",
            "id": "37i9dQZEVXblBkdFy6OJz8",
            "images": [
                {
                    "height": 300,
                    "url": "https://i.scdn.co/image/08d662407a64d6251a4e423913c8ee22a1e27c16",
                    "width": 300
                }
            ],
            "name": "Release Radar",
            "owner": {
                "display_name": "Spotify",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/spotify"
                },
                "href": "https://api.spotify.com/v1/users/spotify",
                "id": "spotify",
                "type": "user",
                "uri": "spotify:user:spotify"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "NDM1LDAwMDAwMDAwZThlZjhiYjM1NDk5ZjYwZjQ3YTI2NTEyZDY0OTBkMDI=",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/37i9dQZEVXblBkdFy6OJz8/tracks",
                "total": 30
            },
            "type": "playlist",
            "uri": "spotify:playlist:37i9dQZEVXblBkdFy6OJz8"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/2IBfKk8Yy9jrLKdzy5BG5I"
            },
            "href": "https://api.spotify.com/v1/playlists/2IBfKk8Yy9jrLKdzy5BG5I",
            "id": "2IBfKk8Yy9jrLKdzy5BG5I",
            "images": [
                {
                    "height": 640,
                    "url": "https://mosaic.scdn.co/640/1a1b2afe397f0294760dbb57e28560ebab6e622f1b79b98976c379aa3d31003f295b47ee0e795e4323e12b9003eebacffb1d10d31b3dcade4db31c7d6a52bff184deae0e4166d298ebc9604876d8117b",
                    "width": 640
                },
                {
                    "height": 300,
                    "url": "https://mosaic.scdn.co/300/1a1b2afe397f0294760dbb57e28560ebab6e622f1b79b98976c379aa3d31003f295b47ee0e795e4323e12b9003eebacffb1d10d31b3dcade4db31c7d6a52bff184deae0e4166d298ebc9604876d8117b",
                    "width": 300
                },
                {
                    "height": 60,
                    "url": "https://mosaic.scdn.co/60/1a1b2afe397f0294760dbb57e28560ebab6e622f1b79b98976c379aa3d31003f295b47ee0e795e4323e12b9003eebacffb1d10d31b3dcade4db31c7d6a52bff184deae0e4166d298ebc9604876d8117b",
                    "width": 60
                }
            ],
            "name": "Electro 2019",
            "owner": {
                "display_name": "Max Kamp",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/1126012545"
                },
                "href": "https://api.spotify.com/v1/users/1126012545",
                "id": "1126012545",
                "type": "user",
                "uri": "spotify:user:1126012545"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "MTksNjkwZjc1OTBjMDliYTAxZDJjMTJlNTAxYmMxNTMxMzBiNmYzNjkwNA==",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/2IBfKk8Yy9jrLKdzy5BG5I/tracks",
                "total": 66
            },
            "type": "playlist",
            "uri": "spotify:playlist:2IBfKk8Yy9jrLKdzy5BG5I"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/1fnKKvPwOrL7YvYmOCCbe0"
            },
            "href": "https://api.spotify.com/v1/playlists/1fnKKvPwOrL7YvYmOCCbe0",
            "id": "1fnKKvPwOrL7YvYmOCCbe0",
            "images": [
                {
                    "height": 640,
                    "url": "https://mosaic.scdn.co/640/2651f47b7dcc4fd5e9f5b13dcbc3f5cbf19e5a93286ac3f2f8703bd8d32aeae43405642d828a0aec8f063ca6032d7ffaa9ee2eb1785e91e6743aec32b01200b5a36464e0e411fc78206da28b21ddb9fc",
                    "width": 640
                },
                {
                    "height": 300,
                    "url": "https://mosaic.scdn.co/300/2651f47b7dcc4fd5e9f5b13dcbc3f5cbf19e5a93286ac3f2f8703bd8d32aeae43405642d828a0aec8f063ca6032d7ffaa9ee2eb1785e91e6743aec32b01200b5a36464e0e411fc78206da28b21ddb9fc",
                    "width": 300
                },
                {
                    "height": 60,
                    "url": "https://mosaic.scdn.co/60/2651f47b7dcc4fd5e9f5b13dcbc3f5cbf19e5a93286ac3f2f8703bd8d32aeae43405642d828a0aec8f063ca6032d7ffaa9ee2eb1785e91e6743aec32b01200b5a36464e0e411fc78206da28b21ddb9fc",
                    "width": 60
                }
            ],
            "name": "Deutschrap 2019",
            "owner": {
                "display_name": "Max Kamp",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/1126012545"
                },
                "href": "https://api.spotify.com/v1/users/1126012545",
                "id": "1126012545",
                "type": "user",
                "uri": "spotify:user:1126012545"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "NDYsZjk5YzJhNjYzMjkyNTc1MTM4YTA3Njc0OTUwMTZiZjJmNTNlZTZhZg==",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/1fnKKvPwOrL7YvYmOCCbe0/tracks",
                "total": 262
            },
            "type": "playlist",
            "uri": "spotify:playlist:1fnKKvPwOrL7YvYmOCCbe0"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/5bTuexMvQSMiUer3hWI74b"
            },
            "href": "https://api.spotify.com/v1/playlists/5bTuexMvQSMiUer3hWI74b",
            "id": "5bTuexMvQSMiUer3hWI74b",
            "images": [
                {
                    "height": 640,
                    "url": "https://mosaic.scdn.co/640/0727bde372abeefbc300361d021b0ee54563c13a307d7a28eac823562b1b65be4f08f3a949ef54e77b6a841b4e6f2c058cf582d82c7c63400d1f2478d9fe8b3cd4d00605f03af6f044d115249ca1af01",
                    "width": 640
                },
                {
                    "height": 300,
                    "url": "https://mosaic.scdn.co/300/0727bde372abeefbc300361d021b0ee54563c13a307d7a28eac823562b1b65be4f08f3a949ef54e77b6a841b4e6f2c058cf582d82c7c63400d1f2478d9fe8b3cd4d00605f03af6f044d115249ca1af01",
                    "width": 300
                },
                {
                    "height": 60,
                    "url": "https://mosaic.scdn.co/60/0727bde372abeefbc300361d021b0ee54563c13a307d7a28eac823562b1b65be4f08f3a949ef54e77b6a841b4e6f2c058cf582d82c7c63400d1f2478d9fe8b3cd4d00605f03af6f044d115249ca1af01",
                    "width": 60
                }
            ],
            "name": "Trap 2019",
            "owner": {
                "display_name": "Max Kamp",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/1126012545"
                },
                "href": "https://api.spotify.com/v1/users/1126012545",
                "id": "1126012545",
                "type": "user",
                "uri": "spotify:user:1126012545"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "MjksNzcxMjQ4ZTZlNTc4ZTZiNTdkYmE2OTE5N2Y1MjVjZTdjNmVlYjEwMw==",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/5bTuexMvQSMiUer3hWI74b/tracks",
                "total": 151
            },
            "type": "playlist",
            "uri": "spotify:playlist:5bTuexMvQSMiUer3hWI74b"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/1Qe19Am0rHK3SqXAYKGC3C"
            },
            "href": "https://api.spotify.com/v1/playlists/1Qe19Am0rHK3SqXAYKGC3C",
            "id": "1Qe19Am0rHK3SqXAYKGC3C",
            "images": [
                {
                    "height": 640,
                    "url": "https://mosaic.scdn.co/640/1a6a9003e212202d0809b066fc0429266f67abbb1b79b98976c379aa3d31003f295b47ee0e795e4323e12b9003eebacffb1d10d31b3dcade4db31c7db642358a03c1453e2f88ca97b1bfc45fc7bccc3e",
                    "width": 640
                },
                {
                    "height": 300,
                    "url": "https://mosaic.scdn.co/300/1a6a9003e212202d0809b066fc0429266f67abbb1b79b98976c379aa3d31003f295b47ee0e795e4323e12b9003eebacffb1d10d31b3dcade4db31c7db642358a03c1453e2f88ca97b1bfc45fc7bccc3e",
                    "width": 300
                },
                {
                    "height": 60,
                    "url": "https://mosaic.scdn.co/60/1a6a9003e212202d0809b066fc0429266f67abbb1b79b98976c379aa3d31003f295b47ee0e795e4323e12b9003eebacffb1d10d31b3dcade4db31c7db642358a03c1453e2f88ca97b1bfc45fc7bccc3e",
                    "width": 60
                }
            ],
            "name": "Techno 2019",
            "owner": {
                "display_name": "Max Kamp",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/1126012545"
                },
                "href": "https://api.spotify.com/v1/users/1126012545",
                "id": "1126012545",
                "type": "user",
                "uri": "spotify:user:1126012545"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "MTgsNmY2MTAzMGMzOGU3ZDVlZTUyZDJjZWQwNWNmZjY1ZDUyYTMzYTVjMQ==",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/1Qe19Am0rHK3SqXAYKGC3C/tracks",
                "total": 30
            },
            "type": "playlist",
            "uri": "spotify:playlist:1Qe19Am0rHK3SqXAYKGC3C"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/4Jlm627QrWKn2ZxVLY23gJ"
            },
            "href": "https://api.spotify.com/v1/playlists/4Jlm627QrWKn2ZxVLY23gJ",
            "id": "4Jlm627QrWKn2ZxVLY23gJ",
            "images": [
                {
                    "height": 640,
                    "url": "https://mosaic.scdn.co/640/0727bde372abeefbc300361d021b0ee54563c13a7b6a841b4e6f2c058cf582d82c7c63400d1f24787c3a5dcf8cacffc4d019478bb8bbdd580dd48114d9fe8b3cd4d00605f03af6f044d115249ca1af01",
                    "width": 640
                },
                {
                    "height": 300,
                    "url": "https://mosaic.scdn.co/300/0727bde372abeefbc300361d021b0ee54563c13a7b6a841b4e6f2c058cf582d82c7c63400d1f24787c3a5dcf8cacffc4d019478bb8bbdd580dd48114d9fe8b3cd4d00605f03af6f044d115249ca1af01",
                    "width": 300
                },
                {
                    "height": 60,
                    "url": "https://mosaic.scdn.co/60/0727bde372abeefbc300361d021b0ee54563c13a7b6a841b4e6f2c058cf582d82c7c63400d1f24787c3a5dcf8cacffc4d019478bb8bbdd580dd48114d9fe8b3cd4d00605f03af6f044d115249ca1af01",
                    "width": 60
                }
            ],
            "name": "Hip Hop 2019",
            "owner": {
                "display_name": "Max Kamp",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/1126012545"
                },
                "href": "https://api.spotify.com/v1/users/1126012545",
                "id": "1126012545",
                "type": "user",
                "uri": "spotify:user:1126012545"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "NDcsZjJmZTg4ODQ4NjQxMWM5ZTM2MDgwN2RiYTlhNzkyOTEwOGFkMTU4ZA==",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/4Jlm627QrWKn2ZxVLY23gJ/tracks",
                "total": 309
            },
            "type": "playlist",
            "uri": "spotify:playlist:4Jlm627QrWKn2ZxVLY23gJ"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/2B7HEFDDeWHyJTYMTU6b84"
            },
            "href": "https://api.spotify.com/v1/playlists/2B7HEFDDeWHyJTYMTU6b84",
            "id": "2B7HEFDDeWHyJTYMTU6b84",
            "images": [
                {
                    "height": 640,
                    "url": "https://mosaic.scdn.co/640/338b7ff72058e1a2907e16abe708fb58fed100d4406e6359a96397f865fe4fb2000ce1ba67238c2e92a8060a905faf5eea7d18660370ae00b1d0d3b1ab67616d0000b273ebca653ec46a3f46db77d528",
                    "width": 640
                },
                {
                    "height": 300,
                    "url": "https://mosaic.scdn.co/300/338b7ff72058e1a2907e16abe708fb58fed100d4406e6359a96397f865fe4fb2000ce1ba67238c2e92a8060a905faf5eea7d18660370ae00b1d0d3b1ab67616d0000b273ebca653ec46a3f46db77d528",
                    "width": 300
                },
                {
                    "height": 60,
                    "url": "https://mosaic.scdn.co/60/338b7ff72058e1a2907e16abe708fb58fed100d4406e6359a96397f865fe4fb2000ce1ba67238c2e92a8060a905faf5eea7d18660370ae00b1d0d3b1ab67616d0000b273ebca653ec46a3f46db77d528",
                    "width": 60
                }
            ],
            "name": "Undefined 2019",
            "owner": {
                "display_name": "Max Kamp",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/1126012545"
                },
                "href": "https://api.spotify.com/v1/users/1126012545",
                "id": "1126012545",
                "type": "user",
                "uri": "spotify:user:1126012545"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "MjI2NCwyYzQxNjNlNzlhMmRhNTI4NDFhZjYxOWY3OGNjNzg5ZjdjYzdiZDA2",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/2B7HEFDDeWHyJTYMTU6b84/tracks",
                "total": 10
            },
            "type": "playlist",
            "uri": "spotify:playlist:2B7HEFDDeWHyJTYMTU6b84"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/3kC7GgZwNZWvYAGvcvwyzC"
            },
            "href": "https://api.spotify.com/v1/playlists/3kC7GgZwNZWvYAGvcvwyzC",
            "id": "3kC7GgZwNZWvYAGvcvwyzC",
            "images": [
                {
                    "height": 640,
                    "url": "https://mosaic.scdn.co/640/0727bde372abeefbc300361d021b0ee54563c13a2651f47b7dcc4fd5e9f5b13dcbc3f5cbf19e5a938f063ca6032d7ffaa9ee2eb1785e91e6743aec32b01200b5a36464e0e411fc78206da28b21ddb9fc",
                    "width": 640
                },
                {
                    "height": 300,
                    "url": "https://mosaic.scdn.co/300/0727bde372abeefbc300361d021b0ee54563c13a2651f47b7dcc4fd5e9f5b13dcbc3f5cbf19e5a938f063ca6032d7ffaa9ee2eb1785e91e6743aec32b01200b5a36464e0e411fc78206da28b21ddb9fc",
                    "width": 300
                },
                {
                    "height": 60,
                    "url": "https://mosaic.scdn.co/60/0727bde372abeefbc300361d021b0ee54563c13a2651f47b7dcc4fd5e9f5b13dcbc3f5cbf19e5a938f063ca6032d7ffaa9ee2eb1785e91e6743aec32b01200b5a36464e0e411fc78206da28b21ddb9fc",
                    "width": 60
                }
            ],
            "name": "Archiv 2019",
            "owner": {
                "display_name": "Max Kamp",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/1126012545"
                },
                "href": "https://api.spotify.com/v1/users/1126012545",
                "id": "1126012545",
                "type": "user",
                "uri": "spotify:user:1126012545"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "MTgxLDZmMTEzZmYxYjY5OGZkOGVlZDdlOWNjNzk1MWNhOWI2ZmZiNzI1ZTc=",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/3kC7GgZwNZWvYAGvcvwyzC/tracks",
                "total": 629
            },
            "type": "playlist",
            "uri": "spotify:playlist:3kC7GgZwNZWvYAGvcvwyzC"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/31PCYFrkYjbpB6D1MELcln"
            },
            "href": "https://api.spotify.com/v1/playlists/31PCYFrkYjbpB6D1MELcln",
            "id": "31PCYFrkYjbpB6D1MELcln",
            "images": [
                {
                    "height": 640,
                    "url": "https://mosaic.scdn.co/640/17ba88a408ba1aa626b193c2748e9e685349ba7b65dc6e172feb8dae430f43e7e1319e4caaf48cd988cb7851b9af9854c0082022c4646d1afe2f6de8ab67616d0000b273dfd64d5db702a33d190f07f0",
                    "width": 640
                },
                {
                    "height": 300,
                    "url": "https://mosaic.scdn.co/300/17ba88a408ba1aa626b193c2748e9e685349ba7b65dc6e172feb8dae430f43e7e1319e4caaf48cd988cb7851b9af9854c0082022c4646d1afe2f6de8ab67616d0000b273dfd64d5db702a33d190f07f0",
                    "width": 300
                },
                {
                    "height": 60,
                    "url": "https://mosaic.scdn.co/60/17ba88a408ba1aa626b193c2748e9e685349ba7b65dc6e172feb8dae430f43e7e1319e4caaf48cd988cb7851b9af9854c0082022c4646d1afe2f6de8ab67616d0000b273dfd64d5db702a33d190f07f0",
                    "width": 60
                }
            ],
            "name": "January 2019",
            "owner": {
                "display_name": "Max Kamp",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/1126012545"
                },
                "href": "https://api.spotify.com/v1/users/1126012545",
                "id": "1126012545",
                "type": "user",
                "uri": "spotify:user:1126012545"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "MjUsOTMyMzU2OTU2YTI2Nzc1MGFkYzFmZTZkNWY1NjRiYjhjZDAzMjM1Yw==",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/31PCYFrkYjbpB6D1MELcln/tracks",
                "total": 24
            },
            "type": "playlist",
            "uri": "spotify:playlist:31PCYFrkYjbpB6D1MELcln"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/2RgKHvpPS0SDJ6oypH9qSZ"
            },
            "href": "https://api.spotify.com/v1/playlists/2RgKHvpPS0SDJ6oypH9qSZ",
            "id": "2RgKHvpPS0SDJ6oypH9qSZ",
            "images": [
                {
                    "height": 640,
                    "url": "https://mosaic.scdn.co/640/2d4c7fee6843cf38ea142032412f03a8402d606b7334a083809f6ea7229c8c61ba00098706250c03809ea0026196e352306c0dbef9cc18766c7baf9da96dc8dfa4c425298f69b29941f1f2a38d541c0c",
                    "width": 640
                },
                {
                    "height": 300,
                    "url": "https://mosaic.scdn.co/300/2d4c7fee6843cf38ea142032412f03a8402d606b7334a083809f6ea7229c8c61ba00098706250c03809ea0026196e352306c0dbef9cc18766c7baf9da96dc8dfa4c425298f69b29941f1f2a38d541c0c",
                    "width": 300
                },
                {
                    "height": 60,
                    "url": "https://mosaic.scdn.co/60/2d4c7fee6843cf38ea142032412f03a8402d606b7334a083809f6ea7229c8c61ba00098706250c03809ea0026196e352306c0dbef9cc18766c7baf9da96dc8dfa4c425298f69b29941f1f2a38d541c0c",
                    "width": 60
                }
            ],
            "name": "February 2019",
            "owner": {
                "display_name": "Max Kamp",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/1126012545"
                },
                "href": "https://api.spotify.com/v1/users/1126012545",
                "id": "1126012545",
                "type": "user",
                "uri": "spotify:user:1126012545"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "ODUsMjI4ZmNkNTIyOWUyNmIyOGI0MjExNTY4MjBmMzVkOTJiNmY0MGRmOA==",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/2RgKHvpPS0SDJ6oypH9qSZ/tracks",
                "total": 82
            },
            "type": "playlist",
            "uri": "spotify:playlist:2RgKHvpPS0SDJ6oypH9qSZ"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/3ClKyzvAofpb9ZDIXEfUTu"
            },
            "href": "https://api.spotify.com/v1/playlists/3ClKyzvAofpb9ZDIXEfUTu",
            "id": "3ClKyzvAofpb9ZDIXEfUTu",
            "images": [
                {
                    "height": 640,
                    "url": "https://mosaic.scdn.co/640/4788d2cb64ecc8402b62afbf48986ec7d20d90b39151c79b8984b63509d51b01c76c7fb39f7d3797ab67616d0000b273b4522d28e78e917b7d09234ab397fec6c9f7d4d67d23974b76a1a795784c52f3",
                    "width": 640
                },
                {
                    "height": 300,
                    "url": "https://mosaic.scdn.co/300/4788d2cb64ecc8402b62afbf48986ec7d20d90b39151c79b8984b63509d51b01c76c7fb39f7d3797ab67616d0000b273b4522d28e78e917b7d09234ab397fec6c9f7d4d67d23974b76a1a795784c52f3",
                    "width": 300
                },
                {
                    "height": 60,
                    "url": "https://mosaic.scdn.co/60/4788d2cb64ecc8402b62afbf48986ec7d20d90b39151c79b8984b63509d51b01c76c7fb39f7d3797ab67616d0000b273b4522d28e78e917b7d09234ab397fec6c9f7d4d67d23974b76a1a795784c52f3",
                    "width": 60
                }
            ],
            "name": "March 2019",
            "owner": {
                "display_name": "Max Kamp",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/1126012545"
                },
                "href": "https://api.spotify.com/v1/users/1126012545",
                "id": "1126012545",
                "type": "user",
                "uri": "spotify:user:1126012545"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "OTksODM4NjRkMzk0YmIwOTM0MDJhOGU4ZmFiMDAyYWQyMDVlZTM0MjgwZg==",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/3ClKyzvAofpb9ZDIXEfUTu/tracks",
                "total": 98
            },
            "type": "playlist",
            "uri": "spotify:playlist:3ClKyzvAofpb9ZDIXEfUTu"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/0uj8KQctJVaZop4XrBa2Ly"
            },
            "href": "https://api.spotify.com/v1/playlists/0uj8KQctJVaZop4XrBa2Ly",
            "id": "0uj8KQctJVaZop4XrBa2Ly",
            "images": [
                {
                    "height": 640,
                    "url": "https://mosaic.scdn.co/640/03e1b618d80ef16976aeb0d9f5a10b98be9e82960935d34d987f3be4cfb98d80398f24b67b7f86cd17b1e41210bb1b7f7de13b8e2226c1ff0012ba786e8917d533d03cfc19503acf918ef4bdb5cd4a74",
                    "width": 640
                },
                {
                    "height": 300,
                    "url": "https://mosaic.scdn.co/300/03e1b618d80ef16976aeb0d9f5a10b98be9e82960935d34d987f3be4cfb98d80398f24b67b7f86cd17b1e41210bb1b7f7de13b8e2226c1ff0012ba786e8917d533d03cfc19503acf918ef4bdb5cd4a74",
                    "width": 300
                },
                {
                    "height": 60,
                    "url": "https://mosaic.scdn.co/60/03e1b618d80ef16976aeb0d9f5a10b98be9e82960935d34d987f3be4cfb98d80398f24b67b7f86cd17b1e41210bb1b7f7de13b8e2226c1ff0012ba786e8917d533d03cfc19503acf918ef4bdb5cd4a74",
                    "width": 60
                }
            ],
            "name": "April 2019",
            "owner": {
                "display_name": "Max Kamp",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/1126012545"
                },
                "href": "https://api.spotify.com/v1/users/1126012545",
                "id": "1126012545",
                "type": "user",
                "uri": "spotify:user:1126012545"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "NTksODNhZjNkNGUyN2UyM2YzODVmMDk2ZDA2MjRjNzljZjg4ZTE5YjI0YQ==",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/0uj8KQctJVaZop4XrBa2Ly/tracks",
                "total": 56
            },
            "type": "playlist",
            "uri": "spotify:playlist:0uj8KQctJVaZop4XrBa2Ly"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/793CmZ4WxQIchAFjIF6HE6"
            },
            "href": "https://api.spotify.com/v1/playlists/793CmZ4WxQIchAFjIF6HE6",
            "id": "793CmZ4WxQIchAFjIF6HE6",
            "images": [
                {
                    "height": 640,
                    "url": "https://mosaic.scdn.co/640/090a421df12c1c91798409f0846a07aab7198412ad5c0623b8a9d11923af24cad2515fc9ec7412b5ef8cf60a2725d4327ac0bfc6f17bb91570826475fe3842197bcfe0903dfb7dd2cd80e7b7cd967132",
                    "width": 640
                },
                {
                    "height": 300,
                    "url": "https://mosaic.scdn.co/300/090a421df12c1c91798409f0846a07aab7198412ad5c0623b8a9d11923af24cad2515fc9ec7412b5ef8cf60a2725d4327ac0bfc6f17bb91570826475fe3842197bcfe0903dfb7dd2cd80e7b7cd967132",
                    "width": 300
                },
                {
                    "height": 60,
                    "url": "https://mosaic.scdn.co/60/090a421df12c1c91798409f0846a07aab7198412ad5c0623b8a9d11923af24cad2515fc9ec7412b5ef8cf60a2725d4327ac0bfc6f17bb91570826475fe3842197bcfe0903dfb7dd2cd80e7b7cd967132",
                    "width": 60
                }
            ],
            "name": "May 2019",
            "owner": {
                "display_name": "Max Kamp",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/1126012545"
                },
                "href": "https://api.spotify.com/v1/users/1126012545",
                "id": "1126012545",
                "type": "user",
                "uri": "spotify:user:1126012545"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "ODAsMDlhOTNiZWMwY2EwOGM4MjQ4YTFmMTZjMWQ5Y2ZmZDU4ODM0ZTI5OQ==",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/793CmZ4WxQIchAFjIF6HE6/tracks",
                "total": 61
            },
            "type": "playlist",
            "uri": "spotify:playlist:793CmZ4WxQIchAFjIF6HE6"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/246bptPdQGg7qTLm81AjvF"
            },
            "href": "https://api.spotify.com/v1/playlists/246bptPdQGg7qTLm81AjvF",
            "id": "246bptPdQGg7qTLm81AjvF",
            "images": [
                {
                    "height": 640,
                    "url": "https://mosaic.scdn.co/640/72564ec21146fe94a9dd584828d8a9c54f2a7b07c2fb6f986574edef63b2333092c217f480baad0fd555fd464895f56810736bebce8945ca766c1567fef692b2edfddfb2db29ec02618c7b41ad783c9d",
                    "width": 640
                },
                {
                    "height": 300,
                    "url": "https://mosaic.scdn.co/300/72564ec21146fe94a9dd584828d8a9c54f2a7b07c2fb6f986574edef63b2333092c217f480baad0fd555fd464895f56810736bebce8945ca766c1567fef692b2edfddfb2db29ec02618c7b41ad783c9d",
                    "width": 300
                },
                {
                    "height": 60,
                    "url": "https://mosaic.scdn.co/60/72564ec21146fe94a9dd584828d8a9c54f2a7b07c2fb6f986574edef63b2333092c217f480baad0fd555fd464895f56810736bebce8945ca766c1567fef692b2edfddfb2db29ec02618c7b41ad783c9d",
                    "width": 60
                }
            ],
            "name": "June 2019",
            "owner": {
                "display_name": "Max Kamp",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/1126012545"
                },
                "href": "https://api.spotify.com/v1/users/1126012545",
                "id": "1126012545",
                "type": "user",
                "uri": "spotify:user:1126012545"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "NDQsMzk3Yjk3YWVkMGQxYTQ0ODFjYzlhOTljYzk4ZGMyMmQ5MmY5YWQ3YQ==",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/246bptPdQGg7qTLm81AjvF/tracks",
                "total": 43
            },
            "type": "playlist",
            "uri": "spotify:playlist:246bptPdQGg7qTLm81AjvF"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/3frBES3w7Z5mTSHLQKx5EI"
            },
            "href": "https://api.spotify.com/v1/playlists/3frBES3w7Z5mTSHLQKx5EI",
            "id": "3frBES3w7Z5mTSHLQKx5EI",
            "images": [
                {
                    "height": 640,
                    "url": "https://mosaic.scdn.co/640/0980d9579b8ff5bb0e9c86c88511dbc3618b0b6b6cfe830816010005c1ac329af43fb0d4a19765a2d31c0053e092274851cdfc867fe2bf8975ef2487efc5ebccc95b03a6e43cd7f7746d59b111b81cb3",
                    "width": 640
                },
                {
                    "height": 300,
                    "url": "https://mosaic.scdn.co/300/0980d9579b8ff5bb0e9c86c88511dbc3618b0b6b6cfe830816010005c1ac329af43fb0d4a19765a2d31c0053e092274851cdfc867fe2bf8975ef2487efc5ebccc95b03a6e43cd7f7746d59b111b81cb3",
                    "width": 300
                },
                {
                    "height": 60,
                    "url": "https://mosaic.scdn.co/60/0980d9579b8ff5bb0e9c86c88511dbc3618b0b6b6cfe830816010005c1ac329af43fb0d4a19765a2d31c0053e092274851cdfc867fe2bf8975ef2487efc5ebccc95b03a6e43cd7f7746d59b111b81cb3",
                    "width": 60
                }
            ],
            "name": "July 2019",
            "owner": {
                "display_name": "Max Kamp",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/1126012545"
                },
                "href": "https://api.spotify.com/v1/users/1126012545",
                "id": "1126012545",
                "type": "user",
                "uri": "spotify:user:1126012545"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "NjQsZGJhNjJhNzUwMTE2ZDQyZjUyNzgxMTI1MzIxMGU4NjZiZGU2M2E4ZQ==",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/3frBES3w7Z5mTSHLQKx5EI/tracks",
                "total": 63
            },
            "type": "playlist",
            "uri": "spotify:playlist:3frBES3w7Z5mTSHLQKx5EI"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/78WH9LuuYW2EmZTO1vP5Cf"
            },
            "href": "https://api.spotify.com/v1/playlists/78WH9LuuYW2EmZTO1vP5Cf",
            "id": "78WH9LuuYW2EmZTO1vP5Cf",
            "images": [
                {
                    "height": 640,
                    "url": "https://mosaic.scdn.co/640/210fa0b8a234b12f66c05c2426dd5a8a23d62dfb40497cc8ed2fe1b6dc3bdf881f104720bcfa31f45f685ee4955bd02928e49455d5a23e1613c6ef40fb56c26ac22e95c6d0c8cced0281df3063d7dfcb",
                    "width": 640
                },
                {
                    "height": 300,
                    "url": "https://mosaic.scdn.co/300/210fa0b8a234b12f66c05c2426dd5a8a23d62dfb40497cc8ed2fe1b6dc3bdf881f104720bcfa31f45f685ee4955bd02928e49455d5a23e1613c6ef40fb56c26ac22e95c6d0c8cced0281df3063d7dfcb",
                    "width": 300
                },
                {
                    "height": 60,
                    "url": "https://mosaic.scdn.co/60/210fa0b8a234b12f66c05c2426dd5a8a23d62dfb40497cc8ed2fe1b6dc3bdf881f104720bcfa31f45f685ee4955bd02928e49455d5a23e1613c6ef40fb56c26ac22e95c6d0c8cced0281df3063d7dfcb",
                    "width": 60
                }
            ],
            "name": "August 2019",
            "owner": {
                "display_name": "Max Kamp",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/1126012545"
                },
                "href": "https://api.spotify.com/v1/users/1126012545",
                "id": "1126012545",
                "type": "user",
                "uri": "spotify:user:1126012545"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "MTA5LDVhNzg5MTQ0NTYzMTUyNTA3ODk1NDdhYjY5ZTc3NTk1MDAwZjdkMDc=",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/78WH9LuuYW2EmZTO1vP5Cf/tracks",
                "total": 108
            },
            "type": "playlist",
            "uri": "spotify:playlist:78WH9LuuYW2EmZTO1vP5Cf"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/6eVeJeaA0pPygjYZU8xnjL"
            },
            "href": "https://api.spotify.com/v1/playlists/6eVeJeaA0pPygjYZU8xnjL",
            "id": "6eVeJeaA0pPygjYZU8xnjL",
            "images": [
                {
                    "height": 640,
                    "url": "https://mosaic.scdn.co/640/6d3d84fed0b082d66664e0eecc1b64897d16985e80c3080b21f1fbbb2479a5ae244d14321c3eda60981bbbf258823494e8c7b2382b51a50ed03bfc80a8aa288b6bd1d10c67d1740dac1ed1cb7dd68d30",
                    "width": 640
                },
                {
                    "height": 300,
                    "url": "https://mosaic.scdn.co/300/6d3d84fed0b082d66664e0eecc1b64897d16985e80c3080b21f1fbbb2479a5ae244d14321c3eda60981bbbf258823494e8c7b2382b51a50ed03bfc80a8aa288b6bd1d10c67d1740dac1ed1cb7dd68d30",
                    "width": 300
                },
                {
                    "height": 60,
                    "url": "https://mosaic.scdn.co/60/6d3d84fed0b082d66664e0eecc1b64897d16985e80c3080b21f1fbbb2479a5ae244d14321c3eda60981bbbf258823494e8c7b2382b51a50ed03bfc80a8aa288b6bd1d10c67d1740dac1ed1cb7dd68d30",
                    "width": 60
                }
            ],
            "name": "September 2019",
            "owner": {
                "display_name": "Max Kamp",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/1126012545"
                },
                "href": "https://api.spotify.com/v1/users/1126012545",
                "id": "1126012545",
                "type": "user",
                "uri": "spotify:user:1126012545"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "MTM1LDBiMmUyZjI2NzBkMzRlMzcyNGZkMWE0NDMxNjIzZTYzMThiN2Y0MTM=",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/6eVeJeaA0pPygjYZU8xnjL/tracks",
                "total": 134
            },
            "type": "playlist",
            "uri": "spotify:playlist:6eVeJeaA0pPygjYZU8xnjL"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/149LCntjZh5b9rcARu3dGV"
            },
            "href": "https://api.spotify.com/v1/playlists/149LCntjZh5b9rcARu3dGV",
            "id": "149LCntjZh5b9rcARu3dGV",
            "images": [
                {
                    "height": 640,
                    "url": "https://i.scdn.co/image/d660c26c1787ed064ec6c32bff4c9d886f69e159",
                    "width": 640
                }
            ],
            "name": "YouTube",
            "owner": {
                "display_name": "Max Kamp",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/1126012545"
                },
                "href": "https://api.spotify.com/v1/users/1126012545",
                "id": "1126012545",
                "type": "user",
                "uri": "spotify:user:1126012545"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "Myw0N2FlMTczZjZmNzAyZDMzNjBjMGUxNTdmYzUwNmRjMDY2MmMxMzk2",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/149LCntjZh5b9rcARu3dGV/tracks",
                "total": 17
            },
            "type": "playlist",
            "uri": "spotify:playlist:149LCntjZh5b9rcARu3dGV"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/7J4TqC5ihltA8v42RTCPth"
            },
            "href": "https://api.spotify.com/v1/playlists/7J4TqC5ihltA8v42RTCPth",
            "id": "7J4TqC5ihltA8v42RTCPth",
            "images": [
                {
                    "height": 640,
                    "url": "https://mosaic.scdn.co/640/33d53914e34c468b7e588daeea208f7d9ec9d2894afdb707e1b5846c94bb5c06d5f3c88c545bf1534ecc050e7259009b332d7fb97d9177e084edc1eaab67616d0000b2738772e10ef2afd6e98e9d3112",
                    "width": 640
                },
                {
                    "height": 300,
                    "url": "https://mosaic.scdn.co/300/33d53914e34c468b7e588daeea208f7d9ec9d2894afdb707e1b5846c94bb5c06d5f3c88c545bf1534ecc050e7259009b332d7fb97d9177e084edc1eaab67616d0000b2738772e10ef2afd6e98e9d3112",
                    "width": 300
                },
                {
                    "height": 60,
                    "url": "https://mosaic.scdn.co/60/33d53914e34c468b7e588daeea208f7d9ec9d2894afdb707e1b5846c94bb5c06d5f3c88c545bf1534ecc050e7259009b332d7fb97d9177e084edc1eaab67616d0000b2738772e10ef2afd6e98e9d3112",
                    "width": 60
                }
            ],
            "name": "Discover Archive",
            "owner": {
                "display_name": "Max Kamp",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/1126012545"
                },
                "href": "https://api.spotify.com/v1/users/1126012545",
                "id": "1126012545",
                "type": "user",
                "uri": "spotify:user:1126012545"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "MTc4LDkxNjdlNjBiZTdmZGEwNzQyYmRjM2JhZDFjNTRmMDM2NDQ2NWYwYjE=",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/7J4TqC5ihltA8v42RTCPth/tracks",
                "total": 177
            },
            "type": "playlist",
            "uri": "spotify:playlist:7J4TqC5ihltA8v42RTCPth"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/5DgIPbZFqiNmUZuZBX8qwV"
            },
            "href": "https://api.spotify.com/v1/playlists/5DgIPbZFqiNmUZuZBX8qwV",
            "id": "5DgIPbZFqiNmUZuZBX8qwV",
            "images": [
                {
                    "height": 640,
                    "url": "https://mosaic.scdn.co/640/33d53914e34c468b7e588daeea208f7d9ec9d2894afdb707e1b5846c94bb5c06d5f3c88c545bf1534ecc050e7259009b332d7fb97d9177e084edc1eaab67616d0000b2738772e10ef2afd6e98e9d3112",
                    "width": 640
                },
                {
                    "height": 300,
                    "url": "https://mosaic.scdn.co/300/33d53914e34c468b7e588daeea208f7d9ec9d2894afdb707e1b5846c94bb5c06d5f3c88c545bf1534ecc050e7259009b332d7fb97d9177e084edc1eaab67616d0000b2738772e10ef2afd6e98e9d3112",
                    "width": 300
                },
                {
                    "height": 60,
                    "url": "https://mosaic.scdn.co/60/33d53914e34c468b7e588daeea208f7d9ec9d2894afdb707e1b5846c94bb5c06d5f3c88c545bf1534ecc050e7259009b332d7fb97d9177e084edc1eaab67616d0000b2738772e10ef2afd6e98e9d3112",
                    "width": 60
                }
            ],
            "name": "All Discover Weekly",
            "owner": {
                "display_name": "Max Kamp",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/1126012545"
                },
                "href": "https://api.spotify.com/v1/users/1126012545",
                "id": "1126012545",
                "type": "user",
                "uri": "spotify:user:1126012545"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "ODc3LGRiZTcxZTlmN2NhZjYxNzEyMzRlNmI3Y2Q1NTIxYTgzZjlhMzAwZWY=",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/5DgIPbZFqiNmUZuZBX8qwV/tracks",
                "total": 876
            },
            "type": "playlist",
            "uri": "spotify:playlist:5DgIPbZFqiNmUZuZBX8qwV"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/22zAQFHmAvH3jLzY9ujSXx"
            },
            "href": "https://api.spotify.com/v1/playlists/22zAQFHmAvH3jLzY9ujSXx",
            "id": "22zAQFHmAvH3jLzY9ujSXx",
            "images": [
                {
                    "height": 640,
                    "url": "https://mosaic.scdn.co/640/101300eb3886c58980789897c1360e92e38f98298a15d3e01053de7d5423cdad06964044e31e3ec3ab67616d0000b2736341980b7f0c51748df5986acdffbafcb1a3f4c112dbeeeb14d4007dbf0c500a",
                    "width": 640
                },
                {
                    "height": 300,
                    "url": "https://mosaic.scdn.co/300/101300eb3886c58980789897c1360e92e38f98298a15d3e01053de7d5423cdad06964044e31e3ec3ab67616d0000b2736341980b7f0c51748df5986acdffbafcb1a3f4c112dbeeeb14d4007dbf0c500a",
                    "width": 300
                },
                {
                    "height": 60,
                    "url": "https://mosaic.scdn.co/60/101300eb3886c58980789897c1360e92e38f98298a15d3e01053de7d5423cdad06964044e31e3ec3ab67616d0000b2736341980b7f0c51748df5986acdffbafcb1a3f4c112dbeeeb14d4007dbf0c500a",
                    "width": 60
                }
            ],
            "name": "Release Radar Archive",
            "owner": {
                "display_name": "Max Kamp",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/1126012545"
                },
                "href": "https://api.spotify.com/v1/users/1126012545",
                "id": "1126012545",
                "type": "user",
                "uri": "spotify:user:1126012545"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "Njk3LDU0NTZhNzA5N2ZiNzYzYjI2N2Q4YWVlNTc2ZDJjZTk1NjU5ZTk4YzQ=",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/22zAQFHmAvH3jLzY9ujSXx/tracks",
                "total": 696
            },
            "type": "playlist",
            "uri": "spotify:playlist:22zAQFHmAvH3jLzY9ujSXx"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/37i9dQZF1DX6VdMW310YC7"
            },
            "href": "https://api.spotify.com/v1/playlists/37i9dQZF1DX6VdMW310YC7",
            "id": "37i9dQZF1DX6VdMW310YC7",
            "images": [
                {
                    "height": null,
                    "url": "https://pl.scdn.co/images/pl/default/bb3e01dc5fda482f2000590b525c03cda4b50d52",
                    "width": null
                }
            ],
            "name": "Chill Tracks",
            "owner": {
                "display_name": "Spotify",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/spotify"
                },
                "href": "https://api.spotify.com/v1/users/spotify",
                "id": "spotify",
                "type": "user",
                "uri": "spotify:user:spotify"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "MTU2OTc2NTk0NiwwMDAwMDAwMGQ0MWQ4Y2Q5OGYwMGIyMDRlOTgwMDk5OGVjZjg0Mjdl",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/37i9dQZF1DX6VdMW310YC7/tracks",
                "total": 200
            },
            "type": "playlist",
            "uri": "spotify:playlist:37i9dQZF1DX6VdMW310YC7"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/37i9dQZF1DX3ND264N08pv"
            },
            "href": "https://api.spotify.com/v1/playlists/37i9dQZF1DX3ND264N08pv",
            "id": "37i9dQZF1DX3ND264N08pv",
            "images": [
                {
                    "height": null,
                    "url": "https://i.scdn.co/image/ab67706f000000021a887050239c38a498c840d0",
                    "width": null
                }
            ],
            "name": "Rage Beats",
            "owner": {
                "display_name": "Spotify",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/spotify"
                },
                "href": "https://api.spotify.com/v1/users/spotify",
                "id": "spotify",
                "type": "user",
                "uri": "spotify:user:spotify"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "MTU2OTc2NTk0NiwwMDAwMDAwMGQ0MWQ4Y2Q5OGYwMGIyMDRlOTgwMDk5OGVjZjg0Mjdl",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/37i9dQZF1DX3ND264N08pv/tracks",
                "total": 136
            },
            "type": "playlist",
            "uri": "spotify:playlist:37i9dQZF1DX3ND264N08pv"
        },
        {
            "collaborative": true,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/0UNF0VKPD3VuxwZBcOOKEX"
            },
            "href": "https://api.spotify.com/v1/playlists/0UNF0VKPD3VuxwZBcOOKEX",
            "id": "0UNF0VKPD3VuxwZBcOOKEX",
            "images": [
                {
                    "height": 640,
                    "url": "https://mosaic.scdn.co/640/1570fcaa4930729f25bacf4e766f25ae5ec6e468196d4322e111f5cf691fd1c7aca01fdc33349db6c15973e4a6029810539bbdcd814ee64f72206273ce5caaaa972ab620a901b2249289f5a7510f54ac",
                    "width": 640
                },
                {
                    "height": 300,
                    "url": "https://mosaic.scdn.co/300/1570fcaa4930729f25bacf4e766f25ae5ec6e468196d4322e111f5cf691fd1c7aca01fdc33349db6c15973e4a6029810539bbdcd814ee64f72206273ce5caaaa972ab620a901b2249289f5a7510f54ac",
                    "width": 300
                },
                {
                    "height": 60,
                    "url": "https://mosaic.scdn.co/60/1570fcaa4930729f25bacf4e766f25ae5ec6e468196d4322e111f5cf691fd1c7aca01fdc33349db6c15973e4a6029810539bbdcd814ee64f72206273ce5caaaa972ab620a901b2249289f5a7510f54ac",
                    "width": 60
                }
            ],
            "name": "TechnoMechno 🦍🐿️💃",
            "owner": {
                "display_name": "Max Kamp",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/1126012545"
                },
                "href": "https://api.spotify.com/v1/users/1126012545",
                "id": "1126012545",
                "type": "user",
                "uri": "spotify:user:1126012545"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "MTMsNmU1ZjE3YTQ2OGIyMjk3ZmJiNTlkMzJjNWJlNWFjNzc0NDRhNDhhNg==",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/0UNF0VKPD3VuxwZBcOOKEX/tracks",
                "total": 10
            },
            "type": "playlist",
            "uri": "spotify:playlist:0UNF0VKPD3VuxwZBcOOKEX"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/37i9dQZF1DXa2SPUyWl8Y5"
            },
            "href": "https://api.spotify.com/v1/playlists/37i9dQZF1DXa2SPUyWl8Y5",
            "id": "37i9dQZF1DXa2SPUyWl8Y5",
            "images": [
                {
                    "height": null,
                    "url": "https://pl.scdn.co/images/pl/default/bed28eb1917d3aefd7a7ae7b8577b629b36195d3",
                    "width": null
                }
            ],
            "name": "Beats to think to",
            "owner": {
                "display_name": "Spotify",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/spotify"
                },
                "href": "https://api.spotify.com/v1/users/spotify",
                "id": "spotify",
                "type": "user",
                "uri": "spotify:user:spotify"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "MTU2OTc2NTk0NiwwMDAwMDAwMGQ0MWQ4Y2Q5OGYwMGIyMDRlOTgwMDk5OGVjZjg0Mjdl",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/37i9dQZF1DXa2SPUyWl8Y5/tracks",
                "total": 80
            },
            "type": "playlist",
            "uri": "spotify:playlist:37i9dQZF1DXa2SPUyWl8Y5"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/6KnQDwp0syvhfHOR4lWP7x"
            },
            "href": "https://api.spotify.com/v1/playlists/6KnQDwp0syvhfHOR4lWP7x",
            "id": "6KnQDwp0syvhfHOR4lWP7x",
            "images": [
                {
                    "height": null,
                    "url": "https://pl.scdn.co/images/pl/default/04d0b77e5e3facbed080f4d03dc30abfed37b335",
                    "width": null
                }
            ],
            "name": "Fitness Workout Electro | House | Dance | Progressive House",
            "owner": {
                "display_name": "Lukas Solenthaler",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/lukas.solenthaler"
                },
                "href": "https://api.spotify.com/v1/users/lukas.solenthaler",
                "id": "lukas.solenthaler",
                "type": "user",
                "uri": "spotify:user:lukas.solenthaler"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "MTAwOSxkODQ0NTQ5NTExODM5MTFjYTlhZjE4ZjVmNTBiYzk4YmI3NzQ0YzVi",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/6KnQDwp0syvhfHOR4lWP7x/tracks",
                "total": 776
            },
            "type": "playlist",
            "uri": "spotify:playlist:6KnQDwp0syvhfHOR4lWP7x"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/1TMjYPVlpU322oBkjo3AWh"
            },
            "href": "https://api.spotify.com/v1/playlists/1TMjYPVlpU322oBkjo3AWh",
            "id": "1TMjYPVlpU322oBkjo3AWh",
            "images": [
                {
                    "height": 640,
                    "url": "https://mosaic.scdn.co/640/25fe437848d52feb29de945aead538dfc7b441846e906472a6211ba85bc1387e8359748817c57847ad700f49c39bbdea873561cd9d4c31789bd41ebcbff4aaf0b25421e040ac3c848e7221a668809467",
                    "width": 640
                },
                {
                    "height": 300,
                    "url": "https://mosaic.scdn.co/300/25fe437848d52feb29de945aead538dfc7b441846e906472a6211ba85bc1387e8359748817c57847ad700f49c39bbdea873561cd9d4c31789bd41ebcbff4aaf0b25421e040ac3c848e7221a668809467",
                    "width": 300
                },
                {
                    "height": 60,
                    "url": "https://mosaic.scdn.co/60/25fe437848d52feb29de945aead538dfc7b441846e906472a6211ba85bc1387e8359748817c57847ad700f49c39bbdea873561cd9d4c31789bd41ebcbff4aaf0b25421e040ac3c848e7221a668809467",
                    "width": 60
                }
            ],
            "name": "housi",
            "owner": {
                "display_name": "j_may_",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/j_may_"
                },
                "href": "https://api.spotify.com/v1/users/j_may_",
                "id": "j_may_",
                "type": "user",
                "uri": "spotify:user:j_may_"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "MjY4LGNlZjE1NWY3MWM4MzU1NTFlZjk0MWEwN2IzNmQ0OGUyMWZhMTg5ZjU=",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/1TMjYPVlpU322oBkjo3AWh/tracks",
                "total": 266
            },
            "type": "playlist",
            "uri": "spotify:playlist:1TMjYPVlpU322oBkjo3AWh"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/0490IJvkIBsIPJUwmEuG83"
            },
            "href": "https://api.spotify.com/v1/playlists/0490IJvkIBsIPJUwmEuG83",
            "id": "0490IJvkIBsIPJUwmEuG83",
            "images": [
                {
                    "height": 640,
                    "url": "https://mosaic.scdn.co/640/02b8232a6f94f5ec474ad1a9717cdfe5f666938b779005b016afb5d226f0ecd27704b434abffcfa8a0b8ff14e272c79221ff1924f109eee9e01432a4cf691c31387cf0a6b758eb8fe694fb8072fdfc63",
                    "width": 640
                },
                {
                    "height": 300,
                    "url": "https://mosaic.scdn.co/300/02b8232a6f94f5ec474ad1a9717cdfe5f666938b779005b016afb5d226f0ecd27704b434abffcfa8a0b8ff14e272c79221ff1924f109eee9e01432a4cf691c31387cf0a6b758eb8fe694fb8072fdfc63",
                    "width": 300
                },
                {
                    "height": 60,
                    "url": "https://mosaic.scdn.co/60/02b8232a6f94f5ec474ad1a9717cdfe5f666938b779005b016afb5d226f0ecd27704b434abffcfa8a0b8ff14e272c79221ff1924f109eee9e01432a4cf691c31387cf0a6b758eb8fe694fb8072fdfc63",
                    "width": 60
                }
            ],
            "name": "techno",
            "owner": {
                "display_name": "Max Kamp",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/1126012545"
                },
                "href": "https://api.spotify.com/v1/users/1126012545",
                "id": "1126012545",
                "type": "user",
                "uri": "spotify:user:1126012545"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "NSxiYjZhNTNkNzcxMWM5MWMxMDhjNTM2ODIyODA4Y2Y4YjlmNTQyZjAz",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/0490IJvkIBsIPJUwmEuG83/tracks",
                "total": 4
            },
            "type": "playlist",
            "uri": "spotify:playlist:0490IJvkIBsIPJUwmEuG83"
        },
        {
            "collaborative": true,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/6dRrF86EWhhdfP0OQ8HkM7"
            },
            "href": "https://api.spotify.com/v1/playlists/6dRrF86EWhhdfP0OQ8HkM7",
            "id": "6dRrF86EWhhdfP0OQ8HkM7",
            "images": [
                {
                    "height": null,
                    "url": "https://pl.scdn.co/images/pl/default/5f1e19b54f474d5f4e2112bd0da7c5ae1cd698fd",
                    "width": null
                }
            ],
            "name": "µ",
            "owner": {
                "display_name": "Dominik Soare",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/1156224045"
                },
                "href": "https://api.spotify.com/v1/users/1156224045",
                "id": "1156224045",
                "type": "user",
                "uri": "spotify:user:1156224045"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "MTk1LGE4OWZhZWIwMWUyMWQzOTdlM2U0Yjk0YjYzMTdhN2I5NWZkMTc3OWY=",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/6dRrF86EWhhdfP0OQ8HkM7/tracks",
                "total": 157
            },
            "type": "playlist",
            "uri": "spotify:playlist:6dRrF86EWhhdfP0OQ8HkM7"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/37i9dQZF1DX6J5NfMJS675"
            },
            "href": "https://api.spotify.com/v1/playlists/37i9dQZF1DX6J5NfMJS675",
            "id": "37i9dQZF1DX6J5NfMJS675",
            "images": [
                {
                    "height": null,
                    "url": "https://pl.scdn.co/images/pl/default/b54d1b97d76f5e3280504160e45a4746e714d83a",
                    "width": null
                }
            ],
            "name": "Techno Bunker",
            "owner": {
                "display_name": "Spotify",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/spotify"
                },
                "href": "https://api.spotify.com/v1/users/spotify",
                "id": "spotify",
                "type": "user",
                "uri": "spotify:user:spotify"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "MTU2OTU0NTIwNCwwMDAwMDBhODAwMDAwMTZkNzAyZjMyNTIwMDAwMDE2Y2ZlZDk1ZTQ0",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/37i9dQZF1DX6J5NfMJS675/tracks",
                "total": 60
            },
            "type": "playlist",
            "uri": "spotify:playlist:37i9dQZF1DX6J5NfMJS675"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/4pVRI5AfZQvE9WLh6ox8JH"
            },
            "href": "https://api.spotify.com/v1/playlists/4pVRI5AfZQvE9WLh6ox8JH",
            "id": "4pVRI5AfZQvE9WLh6ox8JH",
            "images": [
                {
                    "height": 640,
                    "url": "https://mosaic.scdn.co/640/51cc9860a0f2c0b8577e651e83c6fb43f056a67e521570f6786d3e84746a0eb777e9429b4e6d65655870f6029a88fdd10108f6ff9f72a28ab9450211ab67616d0000b273756c95cd83e86404c7555e30",
                    "width": 640
                },
                {
                    "height": 300,
                    "url": "https://mosaic.scdn.co/300/51cc9860a0f2c0b8577e651e83c6fb43f056a67e521570f6786d3e84746a0eb777e9429b4e6d65655870f6029a88fdd10108f6ff9f72a28ab9450211ab67616d0000b273756c95cd83e86404c7555e30",
                    "width": 300
                },
                {
                    "height": 60,
                    "url": "https://mosaic.scdn.co/60/51cc9860a0f2c0b8577e651e83c6fb43f056a67e521570f6786d3e84746a0eb777e9429b4e6d65655870f6029a88fdd10108f6ff9f72a28ab9450211ab67616d0000b273756c95cd83e86404c7555e30",
                    "width": 60
                }
            ],
            "name": "🖤🖤🖤",
            "owner": {
                "display_name": "Jenny",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/24ax10v0k2tzqw6ph9nf2klpu"
                },
                "href": "https://api.spotify.com/v1/users/24ax10v0k2tzqw6ph9nf2klpu",
                "id": "24ax10v0k2tzqw6ph9nf2klpu",
                "type": "user",
                "uri": "spotify:user:24ax10v0k2tzqw6ph9nf2klpu"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "MTExLDQ4NDg0YjkxN2VhYjQyYTc4ZTk2Yjk0NTg0ODYyYjI3ZDU5YzQ5ZmE=",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/4pVRI5AfZQvE9WLh6ox8JH/tracks",
                "total": 110
            },
            "type": "playlist",
            "uri": "spotify:playlist:4pVRI5AfZQvE9WLh6ox8JH"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/4uDIS8xGiSu3SabCM3HtZ5"
            },
            "href": "https://api.spotify.com/v1/playlists/4uDIS8xGiSu3SabCM3HtZ5",
            "id": "4uDIS8xGiSu3SabCM3HtZ5",
            "images": [
                {
                    "height": 640,
                    "url": "https://mosaic.scdn.co/640/2636fa8dfc534ad20bab4be49d4cdf7a545a48d077eb7c17cafe5503afb1916f4938ba07e90e5420815b51f5a3d2b49d0d05fce5a62a6a92f63ac619920fb78c159b3e0f994b6c144340be9d4a7ba6fe",
                    "width": 640
                },
                {
                    "height": 300,
                    "url": "https://mosaic.scdn.co/300/2636fa8dfc534ad20bab4be49d4cdf7a545a48d077eb7c17cafe5503afb1916f4938ba07e90e5420815b51f5a3d2b49d0d05fce5a62a6a92f63ac619920fb78c159b3e0f994b6c144340be9d4a7ba6fe",
                    "width": 300
                },
                {
                    "height": 60,
                    "url": "https://mosaic.scdn.co/60/2636fa8dfc534ad20bab4be49d4cdf7a545a48d077eb7c17cafe5503afb1916f4938ba07e90e5420815b51f5a3d2b49d0d05fce5a62a6a92f63ac619920fb78c159b3e0f994b6c144340be9d4a7ba6fe",
                    "width": 60
                }
            ],
            "name": "stash",
            "owner": {
                "display_name": "Max Kamp",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/1126012545"
                },
                "href": "https://api.spotify.com/v1/users/1126012545",
                "id": "1126012545",
                "type": "user",
                "uri": "spotify:user:1126012545"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "MTAxLDE5Yjg1ZGExODliNGRmNDRmMTY1MGNkYzUyZjZiNDlkYTJiMDJjODU=",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/4uDIS8xGiSu3SabCM3HtZ5/tracks",
                "total": 199
            },
            "type": "playlist",
            "uri": "spotify:playlist:4uDIS8xGiSu3SabCM3HtZ5"
        },
        {
            "collaborative": true,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/1zo97L0kqEux6y8sdsYzJY"
            },
            "href": "https://api.spotify.com/v1/playlists/1zo97L0kqEux6y8sdsYzJY",
            "id": "1zo97L0kqEux6y8sdsYzJY",
            "images": [
                {
                    "height": 640,
                    "url": "https://mosaic.scdn.co/640/4c7faa2e80d1b1dc8716210bf9f2b2944712899d7a25ebabf65503ec888a1242ee5b0ae2c35f23058b75671c743b3200aa273cc25790660f7af9d3209efb0981f4baafd166ae698c194d9479161c3cab",
                    "width": 640
                },
                {
                    "height": 300,
                    "url": "https://mosaic.scdn.co/300/4c7faa2e80d1b1dc8716210bf9f2b2944712899d7a25ebabf65503ec888a1242ee5b0ae2c35f23058b75671c743b3200aa273cc25790660f7af9d3209efb0981f4baafd166ae698c194d9479161c3cab",
                    "width": 300
                },
                {
                    "height": 60,
                    "url": "https://mosaic.scdn.co/60/4c7faa2e80d1b1dc8716210bf9f2b2944712899d7a25ebabf65503ec888a1242ee5b0ae2c35f23058b75671c743b3200aa273cc25790660f7af9d3209efb0981f4baafd166ae698c194d9479161c3cab",
                    "width": 60
                }
            ],
            "name": "Loft",
            "owner": {
                "display_name": "Max Kamp",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/1126012545"
                },
                "href": "https://api.spotify.com/v1/users/1126012545",
                "id": "1126012545",
                "type": "user",
                "uri": "spotify:user:1126012545"
            },
            "primary_color": null,
            "public": true,
            "snapshot_id": "NDIsMjNjYTIwNzQzZWFlZjY3NDQxZjk0NDk4ZjRkNWNiMGU5OTIxN2NiYw==",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/1zo97L0kqEux6y8sdsYzJY/tracks",
                "total": 37
            },
            "type": "playlist",
            "uri": "spotify:playlist:1zo97L0kqEux6y8sdsYzJY"
        },
        {
            "collaborative": true,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/7zTCFzlvC5q38oPrzjO581"
            },
            "href": "https://api.spotify.com/v1/playlists/7zTCFzlvC5q38oPrzjO581",
            "id": "7zTCFzlvC5q38oPrzjO581",
            "images": [
                {
                    "height": null,
                    "url": "https://pl.scdn.co/images/pl/default/3fb008ea436e51f3427debc7a3b9f6cb44e050a4",
                    "width": null
                }
            ],
            "name": "Europa-Park 🥙🦍💩",
            "owner": {
                "display_name": "Max Kamp",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/1126012545"
                },
                "href": "https://api.spotify.com/v1/users/1126012545",
                "id": "1126012545",
                "type": "user",
                "uri": "spotify:user:1126012545"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "NTAsZmU4NWUxZjY4NWZkODk1YWFjZTFmMjJiODUyYjljMzdkZTUxYzEyNw==",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/7zTCFzlvC5q38oPrzjO581/tracks",
                "total": 44
            },
            "type": "playlist",
            "uri": "spotify:playlist:7zTCFzlvC5q38oPrzjO581"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/37i9dQZF1CAqEpKI64rhv7"
            },
            "href": "https://api.spotify.com/v1/playlists/37i9dQZF1CAqEpKI64rhv7",
            "id": "37i9dQZF1CAqEpKI64rhv7",
            "images": [
                {
                    "height": null,
                    "url": "https://lineup-images.scdn.co/YSR-2019_DEFAULT-en.jpg",
                    "width": null
                }
            ],
            "name": "Your Summer Rewind",
            "owner": {
                "display_name": "Spotify",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/spotify"
                },
                "href": "https://api.spotify.com/v1/users/spotify",
                "id": "spotify",
                "type": "user",
                "uri": "spotify:user:spotify"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "MjYwMjMzNTcsMDAwMDAwMDA5MzRhZDMzYzdjNGJjN2Y2OWYwMGUzNDg0YmJlZjcyZA==",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/37i9dQZF1CAqEpKI64rhv7/tracks",
                "total": 50
            },
            "type": "playlist",
            "uri": "spotify:playlist:37i9dQZF1CAqEpKI64rhv7"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/37i9dQZF1EjwfXKCTrkPAJ"
            },
            "href": "https://api.spotify.com/v1/playlists/37i9dQZF1EjwfXKCTrkPAJ",
            "id": "37i9dQZF1EjwfXKCTrkPAJ",
            "images": [
                {
                    "height": null,
                    "url": "https://lineup-images.scdn.co/your-top-songs-2018_DEFAULT-en.jpg",
                    "width": null
                }
            ],
            "name": "Your Top Songs 2018",
            "owner": {
                "display_name": "Spotify",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/spotify"
                },
                "href": "https://api.spotify.com/v1/users/spotify",
                "id": "spotify",
                "type": "user",
                "uri": "spotify:user:spotify"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "MjU3NjA4NjMsMDAwMDAwMDBkNjNmYjExZDY3YTg5ZWE5MDU5ZTMzNzViMTkyNzJmZQ==",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/37i9dQZF1EjwfXKCTrkPAJ/tracks",
                "total": 100
            },
            "type": "playlist",
            "uri": "spotify:playlist:37i9dQZF1EjwfXKCTrkPAJ"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/37i9dQZF1E9GZ0ezB927Ik"
            },
            "href": "https://api.spotify.com/v1/playlists/37i9dQZF1E9GZ0ezB927Ik",
            "id": "37i9dQZF1E9GZ0ezB927Ik",
            "images": [
                {
                    "height": null,
                    "url": "https://lineup-images.scdn.co/your-top-songs-2017_DEFAULT-en.jpg",
                    "width": null
                }
            ],
            "name": "Your Top Songs 2017",
            "owner": {
                "display_name": "Spotify",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/spotify"
                },
                "href": "https://api.spotify.com/v1/users/spotify",
                "id": "spotify",
                "type": "user",
                "uri": "spotify:user:spotify"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "MjUxODI3ODAsMDAwMDAwMDA4YzU3MDdmMzBmNzZiYmFmM2FlZGQ2OWU3MTlkYmVlNQ==",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/37i9dQZF1E9GZ0ezB927Ik/tracks",
                "total": 100
            },
            "type": "playlist",
            "uri": "spotify:playlist:37i9dQZF1E9GZ0ezB927Ik"
        },
        {
            "collaborative": false,
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/1VwC1wsfdopr3Qy0V4V7oL"
            },
            "href": "https://api.spotify.com/v1/playlists/1VwC1wsfdopr3Qy0V4V7oL",
            "id": "1VwC1wsfdopr3Qy0V4V7oL",
            "images": [
                {
                    "height": 640,
                    "url": "https://mosaic.scdn.co/640/597bfc02a450b01b9c23a8cd31c3ef1f4ec9b79eae9963681b99a13e8dd8f625ae0e2bac2e45eee0d761f09e5e5b66906b0c3c46224efac2af0c245ae5b8ef166a96b4d7795c80922592fa99e33e0510",
                    "width": 640
                },
                {
                    "height": 300,
                    "url": "https://mosaic.scdn.co/300/597bfc02a450b01b9c23a8cd31c3ef1f4ec9b79eae9963681b99a13e8dd8f625ae0e2bac2e45eee0d761f09e5e5b66906b0c3c46224efac2af0c245ae5b8ef166a96b4d7795c80922592fa99e33e0510",
                    "width": 300
                },
                {
                    "height": 60,
                    "url": "https://mosaic.scdn.co/60/597bfc02a450b01b9c23a8cd31c3ef1f4ec9b79eae9963681b99a13e8dd8f625ae0e2bac2e45eee0d761f09e5e5b66906b0c3c46224efac2af0c245ae5b8ef166a96b4d7795c80922592fa99e33e0510",
                    "width": 60
                }
            ],
            "name": "EDM Beats",
            "owner": {
                "display_name": "Max Kamp",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/1126012545"
                },
                "href": "https://api.spotify.com/v1/users/1126012545",
                "id": "1126012545",
                "type": "user",
                "uri": "spotify:user:1126012545"
            },
            "primary_color": null,
            "public": false,
            "snapshot_id": "ODIsNDZhNzlhMjQ2YWMzYzEwYjczMDdkZTkxNGE4YWZlZWFiY2Q0NTFkNw==",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/1VwC1wsfdopr3Qy0V4V7oL/tracks",
                "total": 35
            },
            "type": "playlist",
            "uri": "spotify:playlist:1VwC1wsfdopr3Qy0V4V7oL"
        }
    ]