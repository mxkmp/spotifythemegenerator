import React, {useEffect, useState} from 'react';
import {withRouter} from "react-router";
import queryString from 'query-string'
import {LOGIN_SAVE_LOGIN_INFO, useLoginValue} from "../../contexts/LoginContext";
import {spotify} from "../utils/SpotifyApi";
import {Typography} from "@material-ui/core";
import {makeStyles} from "@material-ui/styles";

export const isLoggedIn = (login) => {
    return (login.access_token !== null); //TODO:
};

const useStyles = makeStyles(theme => ({
    root: {
        // backgroundColor: 'green',
        textAlign: 'center',
        verticalAlign: 'middle',
        lineHeight: '10vh'
    },
}));
const LoginHandler = ({history, location} = this.props) => {
    const [login, dispatch] = useLoginValue();
    const [message, setMessage] = useState("processing login information...")
    const classes = useStyles()

    useEffect(() => {
        const {access_token, expires_in, state, token_type} = queryString.parse(location.hash)
        if (access_token && expires_in && state && token_type) {
            spotify.setAccessToken(access_token)
            spotify.getMe().then((res) => {
                dispatch({
                    type: LOGIN_SAVE_LOGIN_INFO,
                    access_token,
                    expires_in,
                    state,
                    token_type,
                    login_date: new Date().getUTCMilliseconds(),
                    user_id: res.id,
                    display_name: res.display_name,
                    email: res.email
                })
                history.replace('/home')
            })
        } else {
            setMessage("Error processing login informations. Wrong answer from spotify!")
        }

    }, [])

    return (
        <Typography className={classes.root}>
            {message}
        </Typography>
    );
};

export default withRouter(LoginHandler);
