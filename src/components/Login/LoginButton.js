import React, {useEffect, useState} from 'react';
import {makeStyles} from "@material-ui/styles";
import {buildUrl} from 'quick-url';
import {withRouter} from "react-router"
import {useLoginValue} from "../../contexts/LoginContext";
import {isLoggedIn} from "./LoginHandler";
import {Button} from '@material-ui/core'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSpotify} from "@fortawesome/free-brands-svg-icons";
import {AuthHandler} from "../Router/PrivateRoute";

const useStyles = makeStyles(theme => ({
    root: {

    },
    button: {

    },
    icon: {}
}));

const clientId = "849d0002cfc543be9ac971a5ff247b3e"; //TODO: entfernen
const scope = ['playlist-read-private', 'playlist-modify-public', 'playlist-modify-private', 'playlist-read-collaborative', 'user-read-email', 'user-read-private'];

const LoginButton = props => {
    const classes = useStyles();
    const [login, dispatch] = useLoginValue()
    const [title, setTitle] = useState("Login");


    useEffect(() => {
        if (login.access_token) {
            setTitle("Logout")
        } else {
            setTitle("Login to Spotify")
        }
    }, [login.access_token]);

    const generateLoginUrl = (clientId, redirectUri) => {
        const url = ['https://accounts.spotify.com/authorize'];
        const params = {
            client_id: clientId,
            response_type: 'token',
            state: 123,
            redirect_uri: redirectUri,
            show_dialog: false,
            scope: scope.join(" ")
        };
        return buildUrl(url, params);
    }

    const buttonHandler = async (e) => {
        if (!isLoggedIn(login)) {
            const origin = window.location.origin;
            window.open(generateLoginUrl(clientId, `${origin}/login/`), '_self')
        } else {
            AuthHandler.signOut(dispatch);
        }
    };

    return (
        <div className={classes.root}>
            <Button className={classes.button} onClick={buttonHandler} size={"medium"} variant={"outlined"}
                    color={"primary"}>
                <FontAwesomeIcon icon={faSpotify}/>&nbsp;&nbsp;{title}
            </Button>
        </div>
    );
};

LoginButton.propTypes = {

};

export default withRouter(LoginButton);
