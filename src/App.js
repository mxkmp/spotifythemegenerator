import React from 'react';
import './App.css';
import HomeView from "./views/HomeView";
import 'semantic-ui-css/semantic.min.css'
import {BrowserRouter as Router, Redirect, Route, Switch} from "react-router-dom";
import {initialState, LoginProvider, LoginReducer} from "./contexts/LoginContext";
import LoginHandler from "./components/Login/LoginHandler";
import {library} from "@fortawesome/fontawesome-svg-core";
import {faSpotify} from "@fortawesome/free-brands-svg-icons";
import PrivateRoute from "./components/Router/PrivateRoute";
import LoginView from "./views/LoginView";


library.add(faSpotify)

function App() {
  return (
      <div className="App" style={{overflow: 'hidden'}}>
        <LoginProvider initialState={initialState} reducer={LoginReducer}>
            <Router>
                <Switch>
                    <Route path={"/login/*"} component={LoginHandler}/>
                    <Route exact path={"/login"} component={LoginView}/>
                    <PrivateRoute exact path={"/home"} component={HomeView}/>
                    <Redirect to={"/login"}/>
                </Switch>
            </Router>
        </LoginProvider>
    </div>
  );
}

export default App;
